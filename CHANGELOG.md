# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [8.3.1] - 2023-11-14

### Changed

- Bumped VOICEVOX version to 0.14.6.

## [8.3.0] - 2023-08-20

### Added

- Added support for Discord's new naming system.

### Fixed

- Fixed nonexistent VOICEVOX image tag.

## [8.2.0] - 2023-06-08

### Added

- Added support for Unicode 14.0 and 15.0 Emojis.

## [8.1.1] - 2023-06-06

### Changed

- Bumped VOICEVOX version to 0.14.7.

## [8.1.0] - 2023-04-07

### Added

- Added LRU caching support for VOICEVOX TTS engine.

### Changed

- Bumped VOICEVOX version to 0.14.4.

## [8.0.0] - 2023-03-18

### Adeed

- Print main repository url in `/info`

### Changed

- Renamed `/status` to `/info`
- Deprecated cogs a.k.a. dot-commands.
- Hide load averages for non-owner users on `/info`.

## [7.0.0] - 2023-03-13

### Added

- Show socket status on `/status`.
- Added system message to forced disconnections of voice connections.
- Added notification message for force disconnect.
- Allow synthesis of messages in the background of playback.

### Changed

- Bumped VOICEVOX version to 0.14.3.
- Changed to automatically disconnect if all remaining users are bots.

### Fixed

- Fixed the width calculation of CJK characters in tabular responses.
- Fixed the handling of forced disconnections of voice connections.

## [6.0.0] - 2022-09-11

### Added

- Added guild level config to not read username before reading the received text.
- Added guild level config to notify user connects and disconnects.
- Added guild level config to speed up reading when the queue holds more than 2 messages.
- Added text-in-voice channel capability.

### Changed

- Bumped VOICEVOX version to 0.13.0.
- Split docker-compose file up to support both js and yaml configuration files properly.
- Now forced command deployments will skip digest comparisons of application commands.

### Fixed

- Fixed internal regexp to support all kind of custom Emojis.
- Fixed schema validation to support all kind of Discord tokens.
- Skip build script with exit code 0 on CI environments.
- Excluded yaml config files from container to fix behavior on non-compose environments.
- Fixed to track forced voice channel movements by guild admins.

## [5.0.0] - 2022-03-20

### Added

- Added YAML config file support. (#59)
- Made utility functions available from JavaScript.
- Added identity function to provide types for config objects.
- New option `volumeDelta` to control the tone of voice relatively on VOICEVOX. (#68)
- Added a package script to check Trinity configuration structure. (#66)
- Automated the deployments of application commands. (#49)

### Changed

- Synchronize voice preset selections between clients on the same guild. (#61)
- Print package version on `/status`.
- Hiding clients not available in the same guild on `/status`.
- Example files rewritten from scratch based on real-world use cases. (#67)
- Bumped VOICEVOX version to 0.11.4.
- Changed the source of VOICEVOX Docker image to follow the upstream.
- Changed ambiguous package script name `configure` to `deploy`.

### Removed

- Removed `speedScale`. Please use `speedDelta` instead.
- Removed `pitchScale`. Please use `pitchDelta` instead.

### Fixed

- Fixed bug that signals were not reaching the Trinity process.

## [4.3.0] - 2022-03-05

### Added

- Added a utility class that makes creating TTS presets easy. (#58)

### Changed

- Bumped VOICEVOX version to 0.11.3.

## [4.2.0] - 2022-02-27

### Added

- Added `NODE_ENV` Dockerfile build argument to control the behavior of pnpm.
- Added ability to execute slash commands. (#30)
- Ported `ping` and `status` as `/status`.
- Ported `join` as `/vc join`.
- Ported `bye` as `/vc bye`.
- Ported `teach` as `/dict teach`.
- Ported `forget` as `/dict forget`.
- New command `/config` to tweak the client's behavior. (#52)
- Added package script `configure` to deploy slash commands to Discord.
- Generate invite links automatically.
- Added hidden command to perform sanity tests. (#45)
- Support password auth on Redis.
- Added a feature allowing `tts` to behave as an user selectable presets. (#41)

### Changed

- Use the provided serializer by Pino to log errors.
- Inject `NODE_ENV` to build context from host on Docker Compose.
- Disabled parallel audio generation on VOICEVOX. (#50)

### Fixed

- Fixed many wrong pronunciations.

## [4.1.1] - 2022-02-05

### Changed

- Bumped VOICEVOX version to 0.10.4.

## [4.1.0] - 2022-01-30

### Changed

- Respect spoilers. (#38)

## [4.0.1] - 2022-01-30

### Fixed

- Apply message filtering when resuming. (#39)
- Ensure to resume to the correct position. (#40)
- Fixed the inconsistent message formatting.

## [4.0.0] - 2022-01-30

### Added

- Resuming voice connections automatically on login. (#12)
- New option `speedDelta` to control the voice over speed relatively on VOICEVOX.
- New option `pitchDelta` to control the voice over pitch relatively on VOICEVOX.
- New option `intoneDelta` to control the tone of voice relatively on VOICEVOX.

### Changed

- Respect the order of the messages. (#33)
- Removed pre-phoneme length to reduce corresponding speed.
- Lengthened post-phoneme length to include trailing consonants.
- Faster retries on VOICEVOX errors.
- Manage TTS data with Redis, allowing preserving states across sessions. (#34)
- Transpile code to JavaScript at build time. (#32)

### Deprecated

- Setting `speedScale` will cause an error in the next major release.
- Setting `pitchScale` will cause an error in the next major release.

### Fixed

- Made sure to skip reading URLs.
- Fixed many wrong pronunciations.
- Fixed alphabetical words being replaced by individual letters. (#35)
- Pinned the version of VOICEVOX on compose to ensure the availability of 雨晴はう. (#37)
- Now the global error handler will be applied correctly on production environments.
