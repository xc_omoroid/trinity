import { useConfig } from "./config/index.js";
import { syncApplicationCommands } from "./interactions/deploy.js";

void useConfig().then((config) => syncApplicationCommands(config, true));
