import type { Guild } from "discord.js";
import type { Dict } from "./index.js";
import {
  type DictEntries,
  getDictEntries,
  setDictEntry,
  removeDictEntry,
  dictEntriesToDict,
} from "./persistent.js";

const customDictDbPrefix = "custom-dict-db-";

export const getCustomDictEntries = (
  guildId: Guild["id"],
): Promise<DictEntries> => getDictEntries(customDictDbPrefix + guildId, "desc");

export const setCustomDictEntry = (
  guildId: Guild["id"],
  from: string,
  to: string,
) => setDictEntry(customDictDbPrefix + guildId, from, to);

export const removeCustomDictEntry = (guildId: Guild["id"], from: string) =>
  removeDictEntry(customDictDbPrefix + guildId, from);

export const getCustomDict = async (guildId: Guild["id"]): Promise<Dict> =>
  dictEntriesToDict(await getCustomDictEntries(guildId));
