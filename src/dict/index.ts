export type Dict = (input: string) => string | Promise<string>;

export const simpleDict: Dict = (string: string) =>
  (
    [
      // Normalize
      ["\u301c", "ー"],
      ["\uff5e", "ー"],
      // Moves
      ["da", "ディーエー"],
      ["上強", "ウエキョウ"],
      ["横強", "ヨコキョウ"],
      ["下強", "シタキョウ"],
      ["sj", "ショウジャン"],
      ["小j", "ショウジャン"],
      ["大j", "ダイジャン"],
      ["空j", "クウジャン"],
      ["空n", "クウエヌ"],
      ["空前", "クウマエ"],
      ["空後", "クウゴ"],
      ["空上", "クウウエ"],
      ["da", "ディーエー"],
      ["空下", "クウシタ"],
      ["nb", "エヌビー"],
      ["横b", "ヨコビー"],
      ["上b", "ウエビー"],
      ["下b", "シタビー"],
      ["確反", "カクハン"],
      ["当て反", "アテハン"],
      ["反確", "ハンカク"],
      ["前隙", "まえすき"],
      ["後隙", "あとすき"],
      ["回避上がり", "かいひあがり"],
      ["3ds", "スリーディーエス"],
      ["3d", "スリーディー"],
      ["wiiu", "ウイーユー"],
      ["wii", "ウィー"],
      ["switch", "スイッチ"],
      ["dx", "デラックス"],
      ["usa", "ユーエスエー"],
      ["pneuma", "プネウマ"],
      ["ousia", "ウーシア"],
      ["logos", "ロゴス"],
      // Fixes
      ["悪さ", "わるさ"],
      ["強ない", "つよない"],
      ["今相手", "いまあいて"],
      ["大泣き", "おおなき"],
      ["最中", "さなか"],
      ["異星", "いせい"],
      ["他の", "ほかの"],
      ["着地", "ちゃくち"],
      ["小技", "こわざ"],
      ["魔人", "まじん"],
      ["使い所", "つかいどころ"],
      ["勝ったら", "かったら"],
      ["撃墜", "げきつい"],
      ["撃", "げき"],
    ] as const
  ).reduce((plain, [kanji, kana]) => plain.split(kanji).join(kana), string);

export const urlDict: Dict = (string: string) =>
  string.replace(/https?:\/\/[\w!?/+\-_~=;.,*&@#$%()'[\]]+/g, "URL省略");

export const complexDict: Dict = (string: string) =>
  (
    [
      // Basics
      [/ー{2,}/g, "ーー"],
      [/(?<![a-zａ-ｚ])[wｗ]{2,}(?![a-zａ-ｚ])/gi, "ワラワラ"],
      [/(?<![a-zａ-ｚ])[wｗ](?![a-zａ-ｚ])/gi, "ワラ"],
      [/(?<!\p{sc=Han})笑{2,}(?!\p{sc=Han}|\p{scx=Hira})/gu, "ワラワラ"],
      [/(?<!\p{sc=Han})笑(?!\p{sc=Han}|\p{scx=Hira})/gu, "ワラ"],
      // Moves
      [/弱(?!\p{sc=Han}|\p{scx=Hira})/gu, "ジャク"],
      [/[eｅ]-?(?:[sｓ][pｐ][oｏ][rｒ][tｔ][sｓ]?|スポーツ)/gi, "イースポーツ"],
      // Fixes
      [/金(?!\p{sc=Han})/gu, "カネ"],
      [/(?<!\p{sc=Han})擦(?=[っらりるれ])/gu, "こす"],
      [/(?<=[8８])[8８]|[8８](?=[8８])/g, "パチ"],
    ] as const
  ).reduce((plain, [regex, kana]) => plain.replace(regex, kana), string);

export const applyDicts = (string: string, ...dicts: Dict[]): Promise<string> =>
  dicts.reduce(
    async (plain, dic) => dic((await plain).toLowerCase()),
    Promise.resolve(string),
  );
