import type { Dict } from "./index.js";
import { logger } from "../utils/logger.js";
import { trinityDb } from "../db/redis.js";

export type Order = "asc" | "desc";
export type DictEntries = [string, string][];

export const getDictEntries = async (
  hash: string,
  weight: Order,
): Promise<DictEntries> => {
  const dictEntries = Object.entries(await trinityDb.hGetAll(hash));

  const weightedDictEntries = dictEntries.sort(([aFrom], [bFrom]) =>
    weight === "asc"
      ? aFrom.length - bFrom.length
      : bFrom.length - aFrom.length,
  );

  logger.trace({ hash }, "Loaded dictionary.");

  return weightedDictEntries;
};

export const setDictEntry = async (hash: string, from: string, to: string) => {
  await trinityDb.hSet(hash, from, to);

  logger.trace({ hash, from, to }, "Set dictionary pair.");
};

export const removeDictEntry = async (hash: string, from: string) => {
  await trinityDb.hDel(hash, from);

  logger.trace({ hash, from }, "Deleted dictionary pair.");
};

const applyDictEntries = (text: string, dictEntries: DictEntries): string => {
  let appliedText = text;

  for (const [from, to] of dictEntries) {
    if (/^[a-z]+$/i.test(from)) {
      appliedText = appliedText
        .split(new RegExp(`(?<![a-z])${from}(?![a-z])`, "i"))
        .join(to);
    } else {
      appliedText = appliedText.split(from).join(to);
    }
  }

  return appliedText;
};

export const dictEntriesToDict =
  (dictEntries: DictEntries): Dict =>
  (text: string) =>
    applyDictEntries(text, dictEntries);
