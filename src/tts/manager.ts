import { getVoiceConnection, joinVoiceChannel } from "@discordjs/voice";
import { ChannelType, Message, type VoiceBasedChannel } from "discord.js";
import { z } from "zod";
import type {
  SingleTTSConfig,
  TTSConfig,
  TTSPresets,
} from "../config/schema.js";
import { formatMessage, getMessageAuthor } from "../utils/formatMessage.js";
import {
  getGuildOverwrites,
  getUserOverwrites,
} from "../db/localOverwrites.js";
import { logger } from "../utils/logger.js";
import { trinityDb } from "../db/redis.js";
import { safeJsonParse } from "../utils/safeJson.js";
import type { Trinity } from "../server.js";
import { TTS } from "./tts.js";

const ttsManagerHash = "trinity-tts-manager";

export const listPresets = (presets: TTSPresets): string[] =>
  presets.map((preset) => preset.label);

export const validatePresetName = (
  name: string,
  presets: TTSPresets,
): boolean => presets.some((preset) => preset.label === name);

type GetVoiceOverwritesOptions = {
  presets: TTSPresets;
  tokenHash: string;
  guildId: string;
  userId?: string;
};

type VoiceOverwrites = {
  configured: boolean;
  defaultVoice: string;
  currentVoice: string;
};

export const getVoiceOverwrites = async (
  options: GetVoiceOverwritesOptions,
): Promise<VoiceOverwrites> => {
  const configuredVoice = (
    options.presets.find((preset) => preset.default === true) ||
    options.presets[0]
  ).label;

  const guildVoice = (
    await getGuildOverwrites({
      tokenHash: options.tokenHash,
      guildId: options.guildId,
    })
  ).voicePreset;

  let configured: boolean;
  let defaultVoice: string;
  let currentVoice: string;

  if (options.userId != null) {
    const clientVoice = (
      await getUserOverwrites({
        tokenHash: options.tokenHash,
        guildId: options.guildId,
        userId: options.userId,
      })
    ).voicePreset;

    defaultVoice = guildVoice || configuredVoice;
    currentVoice = clientVoice || defaultVoice;
    configured = clientVoice != null;
  } else {
    defaultVoice = configuredVoice;
    currentVoice = guildVoice || defaultVoice;
    configured = guildVoice != null;
  }

  return {
    configured,
    defaultVoice,
    currentVoice,
  };
};

type ResolvePresetOptions = Omit<GetVoiceOverwritesOptions, "presets"> & {
  ttsConfig: TTSConfig;
};

const resolvePreset = async (
  options: ResolvePresetOptions,
): Promise<SingleTTSConfig> => {
  if (!Array.isArray(options.ttsConfig)) {
    return options.ttsConfig;
  }

  const { currentVoice } = await getVoiceOverwrites({
    presets: options.ttsConfig,
    tokenHash: options.tokenHash,
    guildId: options.guildId,
    userId: options.userId,
  });

  const preset = options.ttsConfig.find(
    (value) => value.label === currentVoice,
  );

  if (preset) {
    return preset;
  }

  return (
    options.ttsConfig.find((config) => config.default === true) ||
    options.ttsConfig[0]
  );
};

export const ttsStateSchema = z.object({
  voiceChannelId: z.string(),
  textChannelId: z.string(),
  lastAddedMessageId: z.string(),
  lastAddedUserId: z.string().nullable(),
  lastPlayedMessageId: z.string(),
  lastPlayedUserId: z.string().nullable(),
});

export type TTSState = z.infer<typeof ttsStateSchema>;

type TTSInstance = InstanceType<typeof TTS>;

type TTSInstances = Map<string, TTSInstance>;

type TTSData = {
  ttsState: TTSState;
  ttsInstance: TTSInstance;
};

export type TTSManager = {
  get: (guildId: string) => Promise<TTSData | undefined>;
  updateState: (guildId: string, state: Partial<TTSState>) => Promise<TTSState>;
  connect: (
    firstMessageId: string,
    textChannelId: string,
    voiceChannelId: string,
  ) => Promise<TTSData | undefined>;
  resume: () => Promise<void>;
  disconnect: (guildId: string) => Promise<void>;
  addMessage: (message: Message) => Promise<void>;
  addPlainText: (guildId: string, text: string) => Promise<void>;
  remove: (guildId: string) => Promise<boolean>;
  getLengths: () => Promise<{
    stateLength: number;
    instanceLength: number;
  }>;
};

export const createTTSManager = ({
  bang,
  tokenHash,
  client,
  ttsConfig,
}: InstanceType<typeof Trinity>): TTSManager => {
  const ttsStateFallback: TTSState = {
    voiceChannelId: "0",
    textChannelId: "0",
    lastAddedMessageId: "0",
    lastAddedUserId: null,
    lastPlayedMessageId: "0",
    lastPlayedUserId: null,
  };

  const ttsInstances: TTSInstances = new Map();

  const hashField = (guildId: string) => `${tokenHash}-${guildId}`;

  const hGet = (guildId: string) =>
    trinityDb.hGet(ttsManagerHash, hashField(guildId));

  const hSet = (guildId: string, data: string) =>
    trinityDb.hSet(ttsManagerHash, hashField(guildId), data);

  const hDel = (guildId: string) =>
    trinityDb.hDel(ttsManagerHash, hashField(guildId));

  const hScanIterator = () =>
    trinityDb.hScanIterator(ttsManagerHash, {
      MATCH: hashField("*"),
    });

  const getTTSState = async (
    guildId: string,
  ): Promise<TTSState | undefined> => {
    const dataJson = await hGet(guildId);

    const result = ttsStateSchema.safeParse(safeJsonParse(dataJson));

    if (result.success) {
      return result.data;
    }

    if (dataJson != null && result.error) {
      logger.error(
        { guildId, err: result.error },
        "State seems invalid. Resetting...",
      );
      await hDel(guildId);
    }

    return undefined;
  };

  const setTTSState = async (guildId: string, data: Partial<TTSState>) => {
    const prevData = await getTTSState(guildId);
    const merged: TTSState = { ...ttsStateFallback, ...prevData, ...data };

    logger.trace({ merged }, "Setting tts state");

    await hSet(guildId, JSON.stringify(merged));

    return merged;
  };

  const deleteTTSState = async (guildId: string) => {
    logger.trace({ guildId }, "Removing tts state.");
    await hDel(guildId);
  };

  const getAllGuildTTSState = async (): Promise<Record<string, TTSState>> => {
    const states: [string, TTSState][] = [];
    for await (const { field, value } of hScanIterator()) {
      const result = ttsStateSchema.safeParse(safeJsonParse(value));

      if (result.success) {
        states.push([field, result.data]);
      } else if (result.error) {
        logger.warn({ field }, "Encountered an invalid tts state.");
      }
    }

    return Object.fromEntries(states);
  };

  const connectToVoiceChannel = (
    voiceChannel: VoiceBasedChannel,
  ): InstanceType<typeof TTS> => {
    const connection = joinVoiceChannel({
      channelId: voiceChannel.id,
      guildId: voiceChannel.guild.id,
      adapterCreator: voiceChannel.guild.voiceAdapterCreator,
      group: tokenHash,
    });

    logger.info(
      { channelId: voiceChannel.id },
      "Established a voice connection.",
    );

    return new TTS(connection, 60);
  };

  const getTTSData = async (guildId: string) => {
    const ttsState = await getTTSState(guildId);
    const ttsInstance = ttsInstances.get(guildId);

    if (!ttsState || !ttsInstance) {
      return undefined;
    }

    return {
      ttsState,
      ttsInstance,
    };
  };

  const deleteTTSData = async (guildId: string) => {
    await deleteTTSState(guildId);
    return ttsInstances.delete(guildId);
  };

  return {
    connect: async (
      firstMessageId: string,
      textChannelId: string,
      voiceChannelId: string,
    ) => {
      const textChannel = await client.channels.fetch(textChannelId);
      const voiceChannel = await client.channels.fetch(voiceChannelId);

      if (
        !textChannel ||
        (textChannel.type !== ChannelType.GuildText &&
          textChannel.type !== ChannelType.GuildVoice)
      ) {
        logger.error(
          { textChannelId, textChannelType: textChannel?.type },
          "Failed to fetch text channel.",
        );
        return;
      }

      if (!voiceChannel || voiceChannel.type !== ChannelType.GuildVoice) {
        logger.error(
          { voiceChannelId, voiceChannelType: voiceChannel?.type },
          "Failed to fetch voice channel.",
        );
        return;
      }

      if (!textChannel.guild) {
        logger.error({ textChannelId }, "Text channel is outside of guild.");
        return;
      }

      const guildId = textChannel.guild.id;

      const ttsState = await setTTSState(guildId, {
        voiceChannelId: voiceChannel.id,
        textChannelId: textChannel.id,
        lastAddedMessageId: firstMessageId,
        lastPlayedMessageId: firstMessageId,
      });

      logger.debug({ ttsState }, "Set tts state.");

      const voice = await resolvePreset({
        tokenHash,
        guildId,
        ttsConfig,
      });

      const ttsInstance = connectToVoiceChannel(voiceChannel);

      ttsInstances.set(textChannel.guild.id, ttsInstance);

      void ttsInstance.add(["ボイスチャットに、接続しました"], false, voice);

      return {
        ttsState,
        ttsInstance,
      };
    },
    resume: async () => {
      const ttsStates = await getAllGuildTTSState();

      Object.entries(ttsStates).map(async ([field, ttsState]) => {
        const [, guildId] = field.split("-");

        if (!guildId) {
          logger.error({ field }, "Invalid field name,");
          return;
        }

        try {
          const hasTTSInstance = ttsInstances.has(guildId);

          if (!hasTTSInstance) {
            const voiceChannel = await client.channels.fetch(
              ttsState.voiceChannelId,
            );

            if (!voiceChannel || voiceChannel.type !== ChannelType.GuildVoice) {
              logger.error(
                { voiceChannelId: ttsState.voiceChannelId },
                "Failed to fetch voice channel.",
              );
              return;
            }

            if (voiceChannel.members.size === 0) {
              logger.debug(
                { guildId },
                "Will not resuming because the voice channel is empty.",
              );

              logger.info({ guildId }, "Deleting tts state.");
              await deleteTTSState(guildId);
              return;
            }

            logger.trace({ guildId }, "Resuming voice connection...");

            const ttsInstance = connectToVoiceChannel(voiceChannel);
            ttsInstances.set(guildId, ttsInstance);

            logger.info({ guildId }, "Resumed voice connection.");

            const textChannel = await client.channels.fetch(
              ttsState.textChannelId,
            );

            if (
              !textChannel ||
              (textChannel.type !== ChannelType.GuildText &&
                textChannel.type !== ChannelType.GuildVoice)
            ) {
              logger.error(
                { textChannelId: ttsState.textChannelId },
                "Failed to get text channel.",
              );
              return;
            }

            if (ttsState.lastPlayedMessageId) {
              const messages = await textChannel.messages.fetch({
                after: ttsState.lastPlayedMessageId,
              });

              logger.trace(
                { messages: messages.map((message) => message.id) },
                "Fetched messages.",
              );

              let lastUserId = ttsState.lastPlayedUserId;

              const guildOverwrites = await getGuildOverwrites({
                tokenHash,
                guildId,
              });

              for (const message of messages.reverse().values()) {
                const userId = message.author.id;

                if (
                  message.content.startsWith(bang) ||
                  userId === client.user?.id
                ) {
                  return;
                }

                const voice = await resolvePreset({
                  tokenHash,
                  guildId,
                  userId,
                  ttsConfig,
                });

                const addAuthor =
                  guildOverwrites.readUsername && lastUserId !== userId;

                const shouldPrerender =
                  addAuthor && voice.engine === "voicevox";

                if (shouldPrerender) {
                  await ttsInstance.prerender(
                    [`${getMessageAuthor(message)}。`],
                    guildOverwrites.variableReadingSpeed,
                    voice,
                  );
                }

                const read = ttsInstance.add(
                  formatMessage(message, {
                    addAuthor,
                  }),
                  guildOverwrites.variableReadingSpeed,
                  voice,
                );

                await setTTSState(guildId, {
                  lastAddedUserId: message.author.id,
                  lastAddedMessageId: message.id,
                });

                // Awaiting each message because the amount of messages can be HUGE,
                // and will eat up your resources.
                await read;

                lastUserId = message.author.id;

                await setTTSState(guildId, {
                  lastPlayedUserId: message.author.id,
                  lastPlayedMessageId: message.id,
                });
              }
            }
          }
        } catch (error: unknown) {
          logger.error({ err: error }, "Failed to resume to a voice channel.");
          logger.info({ guildId }, "Deleting tts state for sanity.");
          await deleteTTSState(guildId);
        }
      });
    },
    get: getTTSData,
    updateState: setTTSState,
    disconnect: async (guildId) => {
      const ttsData = await getTTSData(guildId);

      if (!ttsData) {
        logger.warn({ guildId: guildId }, "TTS data seems to be empty.");
        return;
      }

      const ttsInstanceDeleted = await deleteTTSData(guildId);

      if (ttsInstanceDeleted) {
        logger.debug("TTS instance deleted.");
      } else {
        logger.warn(
          { guildId },
          "Failed to delete tts instance. You can safely ignore this warning.",
        );
      }

      const connection = getVoiceConnection(guildId, tokenHash);

      if (!connection) {
        logger.warn(
          { guildId },
          "Failed to get voice connection. You can safely ignore this warning.",
        );
        return;
      }

      const { channelId } = connection.joinConfig;

      logger.debug({ channelId }, "Disconnecting from voice channel...");

      try {
        connection.destroy();
      } catch (error: unknown) {
        logger.error({ err: error }, "Failed to destroy voice connection.");
      }
    },
    addMessage: async (message) => {
      if (!message.inGuild()) {
        return;
      }

      const guildId = message.guild.id;
      const userId = message.author.id;
      const ttsData = await getTTSData(message.guild.id);

      if (
        !ttsData ||
        ttsData.ttsState.textChannelId !== message.channel.id ||
        message.content.startsWith(bang) ||
        userId === client.user?.id
      ) {
        return;
      }

      if (!message.cleanContent) {
        // We need to fetch message contents manually in some cases.
        await message.fetch();
      }

      logger.debug(
        {
          author: { tag: message.author.tag, bot: message.author.bot },
          channel: message.channel.name,
          guild: message.guild.name,
          message: message.cleanContent,
        },
        "Adding message to queue.",
      );

      await setTTSState(guildId, {
        lastAddedUserId: userId,
        lastAddedMessageId: message.id,
      });

      const guildOverwrites = await getGuildOverwrites({
        tokenHash,
        guildId,
      });

      const voice = await resolvePreset({
        tokenHash,
        guildId,
        userId,
        ttsConfig,
      });

      const addAuthor =
        guildOverwrites.readUsername &&
        ttsData.ttsState.lastAddedUserId !== userId;

      const shouldPrerender = addAuthor && voice.engine === "voicevox";

      if (shouldPrerender) {
        await ttsData.ttsInstance.prerender(
          [`${getMessageAuthor(message)}。`],
          guildOverwrites.variableReadingSpeed,
          voice,
        );
      }

      await ttsData.ttsInstance.add(
        formatMessage(message, {
          addAuthor,
        }),
        guildOverwrites.variableReadingSpeed,
        voice,
      );

      await setTTSState(guildId, {
        lastPlayedUserId: message.author.id,
        lastPlayedMessageId: message.id,
      });
    },
    addPlainText: async (guildId, text) => {
      const ttsInstance = ttsInstances.get(guildId);

      if (!ttsInstance) {
        logger.warn({ guildId, text }, "No TTS instance found.");
        return;
      }

      const voice = await resolvePreset({
        tokenHash,
        guildId,
        ttsConfig,
      });

      const variableReadingSpeed = (
        await getGuildOverwrites({
          tokenHash,
          guildId,
        })
      ).variableReadingSpeed;

      void ttsInstance.add([text], variableReadingSpeed, voice);
    },
    remove: deleteTTSData,
    getLengths: async () => {
      const ttsStates = await getAllGuildTTSState();
      const ttsInstanceLength = ttsInstances.size;

      return {
        stateLength: Object.keys(ttsStates).length,
        instanceLength: ttsInstanceLength,
      };
    },
  };
};
