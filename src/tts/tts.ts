import {
  AudioPlayer,
  AudioPlayerStatus,
  AudioResource,
  createAudioPlayer,
  createAudioResource,
  VoiceConnection,
} from "@discordjs/voice";
import { AsyncQueue } from "@sapphire/async-queue";
import type { Guild } from "discord.js";
import type { Readable } from "node:stream";
import type { SingleTTSConfig } from "../config/schema.js";
import { getCustomDict } from "../dict/custom.js";
import { applyDicts, complexDict, urlDict, simpleDict } from "../dict/index.js";
import { getEmojiDict } from "../utils/emoji.js";
import { logger } from "../utils/logger.js";
import { decodeRomajiAll } from "../utils/romaji.js";

export type Text = [string, ...string[]];

export type Talk = (
  text: Text,
  speed: number,
  prerender: boolean,
) => Promise<Readable | null>;

type Queue = Promise<void>;

export const TTS = class {
  guildId: Guild["id"];

  player: AudioPlayer;

  playerQueue: InstanceType<typeof AsyncQueue>;

  synthesizerQueue: InstanceType<typeof AsyncQueue>;

  maxMessageLength: number;

  constructor(connection: VoiceConnection, maxMessageLength: number) {
    const player = createAudioPlayer();
    connection.subscribe(player);

    this.player = player;
    this.playerQueue = new AsyncQueue();
    this.synthesizerQueue = new AsyncQueue();
    this.guildId = connection.joinConfig.guildId;
    this.maxMessageLength = maxMessageLength;

    player.on("error", (error) => {
      logger.error({ err: error }, "Error on player");
    });
  }

  async add(
    text: Text,
    variableSpeed: boolean,
    singleTTSConfig: SingleTTSConfig,
  ): Promise<void> {
    await this._add(text, variableSpeed, singleTTSConfig, false);
  }

  async prerender(
    text: Text,
    variableSpeed: boolean,
    singleTTSConfig: SingleTTSConfig,
  ): Promise<void> {
    await this._add(text, variableSpeed, singleTTSConfig, true);
  }

  async _add(
    text: Text,
    variableSpeed: boolean,
    singleTTSConfig: SingleTTSConfig,
    prerender: boolean,
  ): Promise<void> {
    try {
      // Prepare the synthesizer

      let talk: Talk;

      if (singleTTSConfig.engine === "open_jtalk") {
        const { initializeOpenJtalk } = await import("./engines/openJtalk.js");
        talk = await initializeOpenJtalk(singleTTSConfig);
      } else if (singleTTSConfig.engine === "voicevox") {
        const { initializeVoicevox } = await import("./engines/voicevox.js");
        talk = await initializeVoicevox(singleTTSConfig);
      } else {
        logger.error("No valid engine specified in config.");
        throw new Error("Config file invalid.");
      }

      // Format the text to read

      const textReadTo = (await Promise.all(
        text.map(async (str) =>
          (
            await applyDicts(
              str,
              urlDict,
              await getCustomDict(this.guildId),
              await getEmojiDict(),
              decodeRomajiAll,
              simpleDict,
              complexDict,
            )
          ).replace(/\n+/g, " "),
        ),
      )) as Text;

      let remainingCharacters = this.maxMessageLength;
      const slicedText: string[] = [];

      for (const str of textReadTo) {
        if (str.length <= remainingCharacters) {
          slicedText.push(str);
          remainingCharacters -= str.length;
        } else {
          slicedText.push(
            str.slice(0, remainingCharacters).concat("、以下省略。"),
          );
          break;
        }
      }

      // Prepare the resource

      let resource: AudioResource | undefined;

      // Claim queue for synthesizing

      const synthesizeQueue = this.synthesizerQueue.wait();

      // Claim queue for playback

      let playbackQueue: Queue | undefined;

      if (!prerender) {
        playbackQueue = this.playerQueue.wait();
      }

      try {
        // Calculate speech speed based on queue size

        let speedMultiplier = 1;

        if (variableSpeed) {
          const stack = this.playerQueue.remaining;
          const minStack = 2;
          const maxStack = 5;
          const stretchTarget = 0.25;

          speedMultiplier +=
            Math.min(
              Math.max((stack - minStack) * (1 / (maxStack - minStack)), 0),
              1,
            ) * stretchTarget;

          logger.trace(
            {
              multiplier: speedMultiplier,
              remaining: this.playerQueue.remaining,
            },
            "Adjusting reading speed.",
          );
        }

        // Wait for the previous synthesis to finish

        await synthesizeQueue;

        if (prerender) {
          logger.trace({ textReadTo }, "Running in prerender mode…");
        }

        const readable = await talk(textReadTo, speedMultiplier, prerender);

        if (prerender || !readable /* type guard */) {
          logger.trace({ textReadTo }, "Audio prerendered.");
          return;
        }

        resource = createAudioResource(readable);

        logger.debug({ textReadTo }, "Audio generated.");
      } catch (error: unknown) {
        logger.error({
          err: error,
          message: "Caught an unexpected error during synthesis.",
        });
      } finally {
        logger.debug("Shifting synthesizer queue.");
        this.synthesizerQueue.shift();
      }

      try {
        // Wait for the previous playback to end

        await playbackQueue;

        if (!resource) {
          throw new Error("Audio resource not created.");
        }

        logger.info({ textReadTo }, "Playing audio...");

        // Play the audio

        this.player.play(resource);

        await new Promise<void>((resolve) => {
          this.player.once(AudioPlayerStatus.Idle, () => {
            resolve();
            logger.debug({ textReadTo }, "Audio player is idle.");
          });
        });
      } catch (error: unknown) {
        logger.error({
          err: error,
          message: "Caught an unexpected error during playing audio.",
        });
      } finally {
        logger.debug("Shifting player queue.");
        this.playerQueue.shift();
      }
    } catch (error: unknown) {
      logger.error({
        err: error,
        message: "Aborting TTS playback.",
      });
    }
  }
};
