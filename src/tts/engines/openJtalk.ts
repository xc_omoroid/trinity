import { join, dirname } from "node:path";
import { access, readFile, realpath } from "node:fs/promises";
import { constants as fsConstants } from "node:fs";
import { spawnSync } from "node:child_process";
import which from "which";
import { Readable } from "node:stream";
import { trimAll } from "../../utils/trimAll.js";
import { getEnv } from "../../utils/getEnv.js";
import { logger } from "../../utils/logger.js";
import { temporaryFileTask } from "tempy";
import type { Talk } from "../tts.js";
import type { OpenJTalkConfig } from "../../config/schema.js";

export const jtalkPath = getEnv({
  name: "OPEN_JTALK_PATH",
  onFound: (path) => Promise.resolve(path),
  onNotFound: async () => {
    try {
      return await realpath(await which("open_jtalk"));
    } catch (error: unknown) {
      logger.error(
        { err: error },
        trimAll(`
          open_jtalk not found in $PATH.
          If you want to use OpenJTalk, please specify your binary in environment variable "OPEN_JTALK_PATH".
      `),
      );
    }

    return "";
  },
});

export const dicPath = getEnv({
  name: "DIC_PATH",
  onFound: (path) => Promise.resolve(path),

  onNotFound: async () => {
    if ((await jtalkPath) === "") {
      return;
    }

    try {
      const jtalkBinDir = dirname(await jtalkPath);

      const dir = join(
        // /bin/
        jtalkBinDir,
        // /
        "..",
        // /dic
        "dic",
      );

      // If the accessibility check is failed, this promise will be rejected with an Error.
      await access(dir, fsConstants.R_OK);

      return dir;
    } catch (error: unknown) {
      logger.error(
        { err: error },
        trimAll(`
          $OPEN_JTALK_PATH is specified but dictionary directory cannot be found.
          Please specify your path in environment variable "DIC_PATH".
        `),
      );
    }

    return "";
  },
});

export type OpenJTalkOptions = {
  /**
   * Path of dictionary directory.
   *
   * ```console
   * $ open_jtalk -x <dictionary>
   * ```
   */
  dictionary?: string;
  /**
   * Path of HTS voice file.
   *
   * ```console
   * $ open_jtalk -m <htsVoice>
   * ```
   */
  htsVoice?: string;
  /**
   * Audio sampling frequency.
   *
   * ```console
   * $ open_jtalk -s <samplingFrequency>
   * ```
   *
   * @integer
   * @min 1
   */
  samplingFrequency?: number;
  /**
   * HTS engine frame shift in point. `(sampling frequency * frame ms)`
   *
   * ```console
   * $ open_jtalk -p <htsFrame>
   * ```
   *
   * @integer
   * @min 1
   */
  htsFrame?: number;
  /**
   * HTS engine frequency warping parameter `alpha`.
   *
   * ```console
   * $ open_jtalk -a <tone>
   * ```
   *
   * @float
   * @min 0.0
   * @max 1.0
   */
  alpha?: number;
  /**
   * HTS engine post filtering parameter `beta`.
   *
   * ```console
   * $ open_jtalk -b <beta>
   * ```
   *
   * @float
   * @min 0.0
   * @max 1.0
   */
  beta?: number;
  /**
   * Playback speed.
   *
   * ```console
   * $ open_jtalk -r <speed>
   * ```
   *
   * @float
   * @min 0.0
   */
  speed?: number;
  /**
   * Pitch shift in semitones.
   *
   * ```console
   * $ open_jtalk -fm <pitch>
   * ```
   *
   * @float
   */
  pitch?: number;
  /**
   * Voiced / unvoiced threshold.
   *
   * ```console
   * $ open_jtalk -u <unvoicedThreshold>
   * ```
   *
   * @float
   * @min 0.0
   * @max 1.0
   */
  unvoicedThreshold?: number;
  /**
   * HTS vocoder spectrum coefficient.
   *
   * ```console
   * $ open_jtalk -jm <spectrum>
   * ```
   *
   * @float
   * @min 0.0
   */
  spectrum?: number;
  /**
   * HTS vocoder log F0 value.
   *
   * ```console
   * $ open_jtalk -jf <logF0>
   * ```
   *
   * @float
   * @min 0.0
   */
  logF0?: number;
  /**
   * Volume gain in dB.
   *
   * ```console
   * $ open_jtalk -g <gain>
   * ```
   *
   * @float
   */
  gain?: number;
  /**
   * Audio buffer size.
   *
   * ```console
   * $ open_jtalk -z <bufferSize>
   * ```
   *
   * @integer
   * @min 0
   * @max `samplingFrequency`
   */
  bufferSize?: number;
};

type OpenJtalkNativeOption =
  | "x"
  | "m"
  | "s"
  | "p"
  | "a"
  | "b"
  | "r"
  | "fm"
  | "u"
  | "jm"
  | "jf"
  | "g"
  | "z";

const openJtalkNativeOptions: Record<
  keyof OpenJTalkOptions,
  OpenJtalkNativeOption
> = {
  dictionary: "x",
  htsVoice: "m",
  samplingFrequency: "s",
  htsFrame: "p",
  alpha: "a",
  beta: "b",
  speed: "r",
  pitch: "fm",
  unvoicedThreshold: "u",
  spectrum: "jm",
  logF0: "jf",
  gain: "g",
  bufferSize: "z",
} as const;

const nativeOptionName = (key: keyof OpenJTalkOptions): OpenJtalkNativeOption =>
  openJtalkNativeOptions[key];

export const initializeOpenJtalk = async (
  config: OpenJTalkConfig,
): Promise<Talk> => {
  // If the accessibility check is failed, this promise will be rejected with an Error.
  try {
    await access(config.htsvoicePath, fsConstants.R_OK);
  } catch (error: unknown) {
    logger.error(
      trimAll(`
        *.htsvoice file is not accessible.
        Please check your trinity.config.js.
        Exiting...
      `),
    );

    throw error;
  }

  return async (text, speed, prerender) => {
    if (prerender) {
      logger.warn(
        { text },
        "Prerender is unsupported on Open JTalk. Skipping…",
      );
      return null;
    }

    const defaultOptions: OpenJTalkOptions = {
      dictionary: await dicPath,
      htsVoice: config.htsvoicePath,
      alpha: 0.55,
      // We adjust reading speed here.
      speed: 1 * speed,
      gain: 0.2,
    };

    const openJTalkArgs = Object.entries({
      ...defaultOptions /* , ...options */,
    })
      .filter(
        <T>(
          array: [string, T | undefined],
        ): array is [keyof OpenJTalkOptions, T] =>
          array[0] in openJtalkNativeOptions && typeof array[1] !== "undefined",
      )
      .flatMap(([key, value]) => [
        `-${nativeOptionName(key)}`,
        value.toString(),
      ]);

    const stream = new Readable();

    // HACK: We use temporary files because Node fails to read from stdout of a spawned process. (Debian, Ubuntu)
    await temporaryFileTask(
      async (filename) => {
        const opts = [...openJTalkArgs, "-ow", filename];

        logger.trace(`Spawning "${await jtalkPath} ${opts.join(" ")}"`);

        const { error } = spawnSync(await jtalkPath, opts, {
          input: text.join(""),
        });

        if (error) {
          throw error;
        }

        const buffer = await readFile(filename);

        stream.push(buffer);
        stream.push(null);
      },
      {
        extension: "wav",
      },
    );

    return stream;
  };
};

void (async () =>
  logger.info(
    {
      openJtalk: await jtalkPath,
      dictionary: await dicPath,
    },
    "Using paths",
  ))();
