import { URL } from "node:url";
import { got } from "got";
import { z } from "zod";
import { getEnv } from "../../utils/getEnv.js";
import { logger } from "../../utils/logger.js";
import { Readable } from "node:stream";
import type { Talk } from "../tts.js";
import type { VoicevoxConfig } from "../../config/schema.js";
import QuickLRU from "quick-lru";
import objectHash from "object-hash";
import { createHash } from "node:crypto";
import ms from "ms";

export const lruCacheSize = getEnv({
  name: "VOICEVOX_CACHE_SIZE",
  onFound: (size) => {
    const parsed = Number.parseInt(size, 10);

    if (isNaN(parsed)) {
      logger.error(
        "Failed to parse $VOICEVOX_CACHE_SIZE. Disabling LRU cache.",
      );

      return 0;
    }

    if (parsed <= 0) {
      logger.error(
        `$VOICEVOX_CACHE_SIZE must be a number greater than 0, got ${parsed}. Disabling LRU cache.`,
      );

      return 0;
    }

    return parsed;
  },
  onNotFound: () => {
    logger.info("$VOICEVOX_CACHE_MAX_AGE is not set. Disabling LRU cache.");

    return 0;
  },
});

export const lruCacheMaxAge = getEnv({
  name: "VOICEVOX_CACHE_MAX_AGE",
  onFound: (age) => {
    try {
      return ms(age) as number;
    } catch (error: unknown) {
      logger.error(
        "Failed to parse $VOICEVOX_CACHE_MAX_AGE. Falling back to Infinity.",
      );

      return Infinity;
    }
  },
  onNotFound: () => {
    return Infinity;
  },
});

const prerenderLRU =
  lruCacheSize !== 0
    ? new QuickLRU<string, Buffer>({
        maxSize: lruCacheSize,
        maxAge: lruCacheMaxAge,
      })
    : null;

const prerenderResourceDigest = (
  line: string,
  speed: number,
  config: VoicevoxConfig,
): string => {
  const serializedConfig: string = objectHash(config, {
    algorithm: "passthrough",
    unorderedArrays: true,
    unorderedSets: true,
    unorderedObjects: true,
  });

  const resourceDigest = createHash("sha256")
    .update(line + speed.toString() + serializedConfig)
    .digest("hex");

  return resourceDigest;
};

const g = got.extend({
  followRedirect: true,
  // VOICEVOX only supports IPv4
  dnsLookupIpVersion: 4,
  retry: {
    methods: ["GET", "POST"],
    calculateDelay: ({ attemptCount, error }) => {
      const delay =
        (((2 * attemptCount - 1) * 100 + 1000) * (Math.random() * 1.1)) | 0;

      logger.error(
        {
          attemptCount,
          requestUrl: error.request?.requestUrl ?? null,
          statusCode: error.response?.statusCode ?? null,
        },
        `Request failed by ${error.name}. Retrying after ${delay}ms.`,
      );

      return delay;
    },
  },
});

export const voicevoxEndpoint = getEnv({
  name: "VOICEVOX_ENDPOINT",
  onFound: (endpoint) => {
    try {
      return new URL(endpoint);
    } catch (error: unknown) {
      logger.error(
        { url: endpoint },
        "Failed to parse URL. Falling back to http://localhost:50021/",
      );
      return new URL("http://localhost:50021/");
    }
  },
  onNotFound: () => new URL("http://localhost:50021/"),
});

export const connectWavesEndpoint = new URL("/connect_waves", voicevoxEndpoint);

export const indefiniteSpeakerSchema = z.object({
  name: z.string().min(1),
  style: z.string().min(1),
});

export type IndefiniteSpeaker = z.infer<typeof indefiniteSpeakerSchema>;

const speakersEndpoint = new URL("/speakers", voicevoxEndpoint);

const speakersEndpointSchema = z.array(
  z.object({
    name: z.string(),
    speaker_uuid: z.string().uuid(),
    styles: z.array(
      z.object({
        id: z.number().int().nonnegative(),
        name: z.string(),
      }),
    ),
    version: z.string(),
  }),
);

const styleIdCache: Map<string, number> = new Map();

export const getStyleId = async (
  indefiniteSpeaker: IndefiniteSpeaker,
): Promise<number> => {
  const requestingSpeaker = indefiniteSpeakerSchema.parse(indefiniteSpeaker);

  const cache = styleIdCache.get(JSON.stringify(indefiniteSpeaker));

  if (cache) {
    return cache;
  }

  logger.trace("Requesting speakers...");

  const data = await g.get(speakersEndpoint).json();

  logger.trace("Requested speakers.");

  const parsedData = speakersEndpointSchema.parse(data);

  const speaker = parsedData.find(
    (currentSpeaker) => currentSpeaker.name === requestingSpeaker.name,
  );

  if (!speaker) {
    logger.error(
      `Speaker "${requestingSpeaker.name}" not found. Falling back to default.`,
    );
    return 0;
  }

  const style = speaker.styles.find(
    (currentStyle) => currentStyle.name === requestingSpeaker.style,
  );

  if (!style) {
    logger.error(
      `Style "${requestingSpeaker.style}" not found. Falling back to default.`,
    );
    return 0;
  }

  logger.debug({ speaker, style }, "Resolved style id.");

  styleIdCache.set(JSON.stringify(indefiniteSpeaker), style.id);

  return style.id;
};

const moraSchema = z.object({
  text: z.string(),
  consonant: z.string().nullable(),
  consonant_length: z.number().nullable(),
  vowel: z.string(),
  vowel_length: z.number(),
  pitch: z.number(),
});

const audioQuerySchema = z.object({
  accent_phrases: z.array(
    z.object({
      moras: z.array(moraSchema),
      accent: z.number(),
      pause_mora: moraSchema.nullable(),
      is_interrogative: z.boolean().default(false),
    }),
  ),
  speedScale: z.number(),
  pitchScale: z.number(),
  intonationScale: z.number(),
  volumeScale: z.number(),
  prePhonemeLength: z.number(),
  postPhonemeLength: z.number(),
  outputSamplingRate: z.number(),
  outputStereo: z.boolean(),
  kana: z.string().optional(),
});

type AudioQuery = z.infer<typeof audioQuerySchema>;

type TalkOptions = {
  speaker: IndefiniteSpeaker;
  pitchDelta?: number;
  speedDelta?: number;
  intoneDelta?: number;
  volumeDelta?: number;
};

type GetQueryArgs = {
  text: string;
  styleId: number;
  options?: Omit<TalkOptions, "speaker">;
};

export const getQuery = async ({
  text,
  styleId,
  options,
}: GetQueryArgs): Promise<AudioQuery> => {
  logger.trace("Requesting query.");

  const queryEndpoint = new URL("/audio_query", voicevoxEndpoint);

  const data = await g
    .post(queryEndpoint, {
      searchParams: {
        text: text,
        speaker: styleId,
      },
    })
    .json();

  const parsedData = audioQuerySchema.parse(data);

  // Overwrite phoneme lengths
  parsedData.prePhonemeLength = 0;
  parsedData.postPhonemeLength = 0.3;

  if (options?.pitchDelta != null) {
    logger.trace({ delta: options.pitchDelta }, "Changing pitch scale");
    parsedData.pitchScale += options.pitchDelta / 200;
  }

  if (options?.speedDelta != null) {
    logger.trace({ delta: options.speedDelta }, "Changing speed scale");
    parsedData.speedScale += options.speedDelta / 100;
  }

  if (options?.intoneDelta != null) {
    logger.trace({ delta: options.intoneDelta }, "Changing intonation scale");
    parsedData.intonationScale += options.intoneDelta / 10;
  }

  if (options?.volumeDelta != null) {
    logger.trace({ delta: options.volumeDelta }, "Changing volume scale");
    parsedData.volumeScale += options.volumeDelta / 4;
  }

  logger.trace("Requested query.");

  return parsedData;
};

type TTSArgs = {
  query: AudioQuery;
  styleId: number;
};

export const synthesis = async ({
  query,
  styleId,
}: TTSArgs): Promise<Buffer> => {
  logger.trace("Requesting synthesis.");
  const synthesisEndpoint = new URL("/synthesis", voicevoxEndpoint);

  const buffer: Buffer = await g
    .post(synthesisEndpoint, {
      json: query,
      searchParams: {
        speaker: styleId,
      },
    })
    .buffer();

  logger.trace("Requested synthesis.");

  return buffer;
};

export const initializeVoicevox = async (
  config: VoicevoxConfig,
): Promise<Talk> => {
  const styleId = await getStyleId({
    name: config.speakerName,
    style: config.speakerStyle,
  });

  return async (text, speed, prerender) => {
    const buffers: Buffer[] = [];

    if (prerender && prerenderLRU === null) {
      return null;
    }

    const prerenderMode = prerender && prerenderLRU != null;

    if (prerenderMode) {
      logger.trace({ text }, "Running in prerender mode…");
    }

    for (const line of text) {
      const lineDigest = prerenderResourceDigest(line, speed, config);

      const prerendered = prerenderLRU?.get(lineDigest);

      if (prerendered) {
        logger.debug({ line }, "Using prerendered resource.");

        buffers.push(prerendered);

        continue;
      }

      logger.trace({ line }, "Rendering");

      const query = await getQuery({
        text: line,
        styleId,
        options: {
          pitchDelta: config.pitchDelta,
          speedDelta: config.speedDelta,
          intoneDelta: config.intoneDelta,
          volumeDelta: config.volumeDelta,
        },
      });

      // We adjust reading speed here.
      query.speedScale *= speed;

      const audio = await synthesis({
        query,
        styleId,
      });

      if (prerenderMode) {
        prerenderLRU.set(lineDigest, audio);

        logger.trace({ lineDigest, line }, "Cached prerendered resource.");

        continue;
      }

      buffers.push(audio);
    }

    logger.trace("Rendered all voicevox buffer.");

    if (prerenderMode) {
      return null;
    }

    let connectedWave: Buffer | undefined;

    if (buffers.length === 1) {
      connectedWave = buffers[0]!; // Sorry for the non-null assertion but tsc is such a dumbass.
    } else if (buffers.length > 1) {
      connectedWave = await g
        .post(connectWavesEndpoint, {
          json: buffers.map((buffer) => buffer.toString("base64")),
        })
        .buffer();
    }

    const stream = new Readable();

    if (connectedWave) {
      stream.push(connectedWave);
    }

    stream.push(null);

    return stream;
  };
};

void (async () => {
  logger.info(
    {
      voicevoxEndpoint: voicevoxEndpoint,
      speakersEndpoint: speakersEndpoint,
    },
    "Using endpoints",
  );

  const data = await g.get(speakersEndpoint).json();

  const parsedData = speakersEndpointSchema.parse(data);

  const speakerList = parsedData.map((speaker) => ({
    name: speaker.name,
    styles: speaker.styles,
  }));

  logger.trace(speakerList, "Available speakers");
})();
