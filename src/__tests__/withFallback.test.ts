import { describe, it, expect } from "vitest";
import { withFallback } from "../utils/withFallback.js";

describe("withFallback", () => {
  it("replaces nullish values with its non-nullish fallback", () => {
    expect(
      withFallback(
        {
          foo: "bar",
          foobar: "baz",
          corge: null,
          fred: null,
          plugh: false,
        },
        {
          foo: "qux",
          foobar: "quux",
          corge: "grault",
          garply: "waldo",
        },
      ),
    ).toEqual({
      foo: "bar",
      foobar: "baz",
      corge: "grault",
      garply: "waldo",
      fred: null,
      plugh: false,
    });
  });
});
