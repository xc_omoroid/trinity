import { describe, it, expect } from "vitest";
import { applyDicts, complexDict, simpleDict, urlDict } from "../dict/index.js";

describe("simpleDict", () => {
  it("can convert single known word into kana", () => {
    expect(simpleDict("下強を当てる")).toBe("シタキョウを当てる");
  });

  it("can convert all known words into kana", () => {
    expect(simpleDict("上強下強を当てる")).toBe("ウエキョウシタキョウを当てる");
  });
});

describe("complexDict", () => {
  it("can convert known word into kana", () => {
    expect(complexDict("ｗｗｗ")).toBe("ワラワラ");
  });

  it("can convert all known words into kana", () => {
    expect(urlDict("https://example.com/www.html")).toBe("URL省略");
    expect(complexDict("ｗｗｗ")).toBe("ワラワラ");
  });
});

describe("applyDicts", () => {
  it("applies given dicts", async () => {
    await expect(
      applyDicts("上強下強ｗｗｗ", simpleDict, complexDict),
    ).resolves.toBe("ウエキョウシタキョウワラワラ");
  });
});
