import { describe, it, expect, beforeAll, afterAll } from "vitest";
import {
  getEmojiDict,
  prepareEmojiStore,
  removeVariationSelector,
  sanitizeCustomEmoji,
} from "../utils/emoji.js";
import { closeDb, openDb } from "../db/redis.js";

const detective = "\u{1f575}";
const variationSelector = "\ufe0f";

beforeAll(() => openDb());
afterAll(() => closeDb());

beforeAll(() => prepareEmojiStore());

describe("emoji", () => {
  it("can sanitize custom discord emoji", () => {
    expect(
      sanitizeCustomEmoji(
        "イルカセラピー <:iruka_no_iruka_kun:822060540712189952>",
      ),
    ).toBe("イルカセラピー iruka_no_iruka_kun");
  });

  it("can convert emojis to its readings", async () => {
    const emojiDict = await getEmojiDict();

    // ugly hack
    const result = await emojiDict(
      removeVariationSelector(`巨大な${detective}${variationSelector}`),
    );

    expect(result).toBe("巨大な探偵");
  });

  it("leaves numbers", async () => {
    const emojiDict = await getEmojiDict();

    // ugly hack
    const result = await emojiDict(
      removeVariationSelector(`巨大な10人の${detective}${variationSelector}`),
    );

    expect(result).toBe("巨大な10人の探偵");
  });
});
