import { describe, it, expect } from "vitest";
import { safeJsonParse } from "../utils/safeJson.js";

describe("safeJson", () => {
  it("parses json strings", () => {
    expect(safeJsonParse('{"a":1}')).toEqual({ a: 1 });
  });

  it("returns undefined on invalid json strings", () => {
    expect(safeJsonParse("{{}a:1")).toBe(undefined);
  });
});
