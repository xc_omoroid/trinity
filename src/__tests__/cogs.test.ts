import { describe, it, expect, vi } from "vitest";
import type { Message } from "discord.js";
import { Cogs } from "../cogs.js";

const commandFoo = {
  help: "ほげ",
  callback: () => undefined,
} as const;

const fakeMessage = (text: string) =>
  ({
    content: text,
    cleanContent: text,
    reply: () => undefined,
  }) as unknown as Message;

describe("Cogs", () => {
  it("constructs", () => {
    expect(new Cogs("!monado")).toBeTruthy();
  });

  it("registers command", () => {
    const cogs = new Cogs("!monado");

    cogs.register("foo", commandFoo);

    expect(cogs.commands.get("foo")).toStrictEqual(commandFoo);
  });

  it("unregisters command", () => {
    const cogs = new Cogs("!monado");

    cogs.register("foo", commandFoo);

    expect(cogs.commands.get("foo")).toStrictEqual(commandFoo);

    cogs.unregister("foo");

    expect(cogs.commands.get("foo")).toBeUndefined();
  });

  it("returns help", () => {
    const cogs = new Cogs("!monado");

    cogs.register("foo", commandFoo);

    expect(cogs.help("foo")).toMatch(commandFoo.help);

    cogs.register("bar", { callback: () => undefined });

    expect(cogs.help("bar")).toBeUndefined();
  });

  it("returns command list", () => {
    const cogs = new Cogs("!monado");

    cogs.register("foo", { callback: () => undefined });
    cogs.register("bar", { help: "ふが", callback: () => undefined });
    cogs.register("baz", { help: "ぴよ", callback: () => undefined });

    expect(cogs.list()).toStrictEqual([
      { name: "foo" },
      { name: "bar", help: "ふが" },
      { name: "baz", help: "ぴよ" },
    ]);
  });

  it("handles command message", async () => {
    const commandWithNoArgs = vi.fn((_message) => undefined);
    const commandWithArgs = vi.fn((_message, _args) => undefined);

    const cogs = new Cogs("!monado");

    cogs.register("bar", { callback: commandWithNoArgs });
    cogs.register("baz", { callback: commandWithArgs });

    // valid
    const messageA = fakeMessage("!monado bar");

    // invalid
    const messageB = fakeMessage("!monado bar 123");

    // valid
    const messageC = fakeMessage("!monado baz 123");

    await cogs.handler(messageA);
    await cogs.handler(messageB);
    await cogs.handler(messageC);

    expect(commandWithNoArgs).toHaveBeenCalledTimes(1);
    expect(commandWithNoArgs).toHaveBeenCalledWith(messageA);

    expect(commandWithArgs).toHaveBeenCalledTimes(1);
    expect(commandWithArgs).toHaveBeenCalledWith(messageC, "123");
  });

  it("ignores normal message", async () => {
    const callback = vi.fn();

    const cogs = new Cogs("!monado");

    cogs.register("bar", { callback });

    await cogs.handler(fakeMessage("no-op"));

    expect(callback).not.toHaveBeenCalled();
  });

  it("runs wildcard cog when encounters unknown command", async () => {
    const foo = vi.fn();
    const wildcard = vi.fn();

    const cogs = new Cogs("!monado");

    cogs.register("foo", { callback: foo });
    cogs.register("*", { callback: wildcard });

    await cogs.handler(fakeMessage("!monado bar"));

    expect(foo).not.toHaveBeenCalled();
    expect(wildcard).toHaveBeenCalled();
  });
});
