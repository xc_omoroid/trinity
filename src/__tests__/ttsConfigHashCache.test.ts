import { describe, it, expect } from "vitest";
import { hashFromTTSConfig } from "../db/ttsConfigHashCache.js";
import { Presets } from "../utils.js";

describe("ttsConfigHashCache", () => {
  it("creates the same hash regardless of the order", () => {
    expect(
      hashFromTTSConfig([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    ).toEqual(
      hashFromTTSConfig([
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    );
  });

  it("creates the same hash regardless of the default", () => {
    expect(
      hashFromTTSConfig([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    ).toEqual(
      hashFromTTSConfig([
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    );

    expect(
      hashFromTTSConfig([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    ).toEqual(
      hashFromTTSConfig([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
          default: true,
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    );

    expect(
      hashFromTTSConfig([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
          default: true,
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    ).toEqual(
      hashFromTTSConfig([
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
          default: true,
        },
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
          default: true,
        },
      ]),
    );
  });

  it("creates the same hash for plain arrays", () => {
    expect(
      hashFromTTSConfig(
        new Presets([
          {
            label: "四国めたん",
            engine: "voicevox",
            speakerName: "四国めたん",
            speakerStyle: "ノーマル",
          },
          {
            label: "ずんだもん",
            engine: "voicevox",
            speakerName: "ずんだもん",
            speakerStyle: "ノーマル",
          },
          {
            label: "mei",
            engine: "open_jtalk",
            htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
          },
        ] as const),
      ),
    ).toEqual(
      hashFromTTSConfig([
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]),
    );
  });

  it("creates different hashes for different configurations", () => {
    expect(
      hashFromTTSConfig([
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
      ]),
    ).not.toEqual(
      hashFromTTSConfig([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
          default: true,
        },
      ]),
    );
  });
});
