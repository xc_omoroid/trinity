import { describe, it, expect, beforeAll, afterAll } from "vitest";
import type { Message } from "discord.js";
import { formatMessage } from "../utils/formatMessage.js";
import { closeDb, openDb } from "../db/redis.js";

const message = {
  author: {
    username: "Carlos Ray Norris",
  },
  cleanContent: "You're messing\nwith the wrong guy!",
};

const messageWithNickname = {
  author: {
    username: "Carlos Ray Norris",
  },
  cleanContent: "You're messing\nwith the wrong guy!",
  guild: {
    members: {
      resolve: (_: unknown) => ({ displayName: "Chuck Norris" }),
    },
  },
};

beforeAll(() => openDb());
afterAll(() => closeDb());

describe("formatMessage", () => {
  it("can format message", () => {
    expect(formatMessage(message as Message)).toMatchInlineSnapshot(`
      [
        "You're messing
      with the wrong guy!。",
      ]
    `);

    expect(formatMessage(messageWithNickname as unknown as Message))
      .toMatchInlineSnapshot(`
        [
          "You're messing
        with the wrong guy!。",
        ]
      `);
  });

  it("can format message with username", () => {
    expect(formatMessage(message as Message, { addAuthor: true }))
      .toMatchInlineSnapshot(`
        [
          "Carlos Ray Norris。",
          "You're messing
        with the wrong guy!。",
        ]
      `);
  });

  it("can format message with nickname", () => {
    expect(
      formatMessage(messageWithNickname as unknown as Message, {
        addAuthor: true,
      }),
    ).toMatchInlineSnapshot(`
      [
        "Chuck Norris。",
        "You're messing
      with the wrong guy!。",
      ]
    `);
  });
});
