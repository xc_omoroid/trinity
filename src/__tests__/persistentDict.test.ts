import { describe, it, expect } from "vitest";
import { dictEntriesToDict } from "../dict/persistent.js";

describe("persistentDict", () => {
  it("replaces words based on given entries", () => {
    const testDict = dictEntriesToDict([
      ["写真", "シャシン"],
      ["hs", "ハイスラ"],
    ]);

    expect(testDict("カレンダー写真カレンダー")).toBe(
      "カレンダーシャシンカレンダー",
    );

    expect(testDict("横hs")).toBe("横ハイスラ");

    expect(testDict("2hs")).toBe("2ハイスラ");

    expect(testDict("hsl")).toBe("hsl");
  });
});
