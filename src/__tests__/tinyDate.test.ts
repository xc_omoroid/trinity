import { describe, it, expect } from "vitest";
import { second, minute, hour, day, tinyFormat } from "../utils/tinyDate.js";

describe("tinyDate", () => {
  it("exports 1 second in ms", () => {
    expect(second).toBe(new Date(0).setUTCSeconds(1));
  });
  it("exports 1 minute in ms", () => {
    expect(minute).toBe(new Date(0).setUTCMinutes(1));
  });
  it("exports 1 hour in ms", () => {
    expect(hour).toBe(new Date(0).setUTCHours(1));
  });
  it("exports 1 day in ms", () => {
    expect(day).toBe(new Date(0).setUTCDate(2));
  });

  it("formats milliseconds", () => {
    expect(tinyFormat(3 * second)).toBe("3s");
    expect(tinyFormat(3 * minute)).toBe("3m 0s");
    expect(tinyFormat(3 * hour)).toBe("3h 0m 0s");
    expect(tinyFormat(3 * day)).toBe("3d 0h 0m 0s");
    expect(tinyFormat(2 * day + 3 * hour + 4 * minute + 5 * second)).toBe(
      "2d 3h 4m 5s",
    );
  });
});
