import { describe, it, expect } from "vitest";
import { trimAll } from "../utils/trimAll.js";

describe("trimAll", () => {
  it("trims multiline comment", () => {
    expect(
      trimAll(`
a
 b
  c
    d         ${/* placeholder */ ""}
        `),
    ).toBe("a\nb\nc\nd");
  });
});
