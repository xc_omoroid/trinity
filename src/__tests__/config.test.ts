import { describe, it, expect } from "vitest";
import { trinityConfig } from "../config/schema.js";

describe("config", () => {
  it("can parse configuration file", () => {
    expect(
      trinityConfig.parse({
        tts: {
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
        clients: [
          {
            command: ".test",
            token: "j".repeat(59),
          },
          {
            command: ".test2",
            token: "e".repeat(59),
          },
        ],
      }),
    ).toBeTruthy();

    expect(
      trinityConfig.parse({
        clients: [
          {
            command: ".test",
            token: "j".repeat(59),
            tts: {
              engine: "voicevox",
              speakerName: "四国めたん",
              speakerStyle: "ノーマル",
              speedDelta: 2,
            },
          },
          {
            command: ".test2",
            token: "e".repeat(59),
            tts: {
              engine: "voicevox",
              speakerName: "春日部つむぎ",
              speakerStyle: "ノーマル",
              speedDelta: 2,
            },
          },
        ],
      }),
    ).toBeTruthy();

    expect(
      trinityConfig.parse({
        tts: {
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
        clients: [
          {
            command: ".test",
            token: "j".repeat(59),
            tts: {
              engine: "voicevox",
              speakerName: "四国めたん",
              speakerStyle: "ノーマル",
              speedDelta: 2,
            },
          },
          {
            command: ".test2",
            token: "e".repeat(59),
          },
        ],
      }),
    ).toBeTruthy();
  });

  it("can throw on invalid configs", () => {
    expect(() =>
      trinityConfig.parse({
        clients: [
          {
            command: ".test",
            token: "j".repeat(59),
            tts: {
              engine: "voicevox",
              speakerName: "四国めたん",
              speakerStyle: "ノーマル",
              speedDelta: 2,
            },
          },
          {
            command: "",
            token: "e".repeat(59),
            tts: {
              engine: "voicevox",
              speakerName: "春日部つむぎ",
              speakerStyle: "ノーマル",
              speedDelta: 2,
            },
          },
        ],
      }),
    ).toThrow();

    expect(() =>
      trinityConfig.parse({
        clients: [
          {
            command: ".test",
            token: "j".repeat(59),
          },
          {
            command: ".test2",
            token: "e".repeat(59),
          },
        ],
      }),
    ).toThrow();
  });
});
