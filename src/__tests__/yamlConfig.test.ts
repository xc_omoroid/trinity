import { describe, it, expect } from "vitest";
import { parseYamlConfig } from "../config/yaml.js";

describe("yamlConfig", () => {
  it("can parse configuration file", () => {
    expect(
      parseYamlConfig(`
tts:
  engine: "open_jtalk"
  htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
clients:
  - command: ".test"
    token: "."
  - command: ".test2"
    token: "."`),
    ).toMatchInlineSnapshot(`
      {
        "clients": [
          {
            "command": ".test",
            "token": ".",
          },
          {
            "command": ".test2",
            "token": ".",
          },
        ],
        "tts": {
          "engine": "open_jtalk",
          "htsvoicePath": "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      }
    `);
  });

  it("can parse configuration file with presets", () => {
    expect(
      parseYamlConfig(`
!pset
global_presets:
  - label: "mei"
    engine: "open_jtalk"
    htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
  - label: "四国めたん"
    engine: "voicevox"
    speakerName: "四国めたん"
    speakerStyle: "ノーマル"

---

clients:
  - command: ".test"
    token: "."
    tts: !pget
      from: global_presets
  - command: ".test2"
    token: "."
    tts: !pget
      default: "四国めたん"
      from: global_presets`),
    ).toMatchInlineSnapshot(`
      {
        "clients": [
          {
            "command": ".test",
            "token": ".",
            "tts": Presets [
              {
                "engine": "open_jtalk",
                "htsvoicePath": "/var/open_jtalk/voice/mei_normal.htsvoice",
                "label": "mei",
              },
              {
                "engine": "voicevox",
                "label": "四国めたん",
                "speakerName": "四国めたん",
                "speakerStyle": "ノーマル",
              },
            ],
          },
          {
            "command": ".test2",
            "token": ".",
            "tts": Presets [
              {
                "default": false,
                "engine": "open_jtalk",
                "htsvoicePath": "/var/open_jtalk/voice/mei_normal.htsvoice",
                "label": "mei",
              },
              {
                "default": true,
                "engine": "voicevox",
                "label": "四国めたん",
                "speakerName": "四国めたん",
                "speakerStyle": "ノーマル",
              },
            ],
          },
        ],
      }
    `);

    expect(
      parseYamlConfig(`
!pset
global_presets:
  - label: "mei"
    engine: "open_jtalk"
    htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
  - label: "四国めたん"
    engine: "voicevox"
    speakerName: "四国めたん"
    speakerStyle: "ノーマル"

---

clients:
  - command: ".test"
    token: "."
    tts: !pget
      from: global_presets
      choices: mei
  - command: ".test2"
    token: "."
    tts: !pget
      choices:
        - mei
        - 四国めたん
      from: global_presets`),
    ).toMatchInlineSnapshot(`
      {
        "clients": [
          {
            "command": ".test",
            "token": ".",
            "tts": Presets [
              {
                "engine": "open_jtalk",
                "htsvoicePath": "/var/open_jtalk/voice/mei_normal.htsvoice",
                "label": "mei",
              },
            ],
          },
          {
            "command": ".test2",
            "token": ".",
            "tts": Presets [
              {
                "engine": "open_jtalk",
                "htsvoicePath": "/var/open_jtalk/voice/mei_normal.htsvoice",
                "label": "mei",
              },
              {
                "engine": "voicevox",
                "label": "四国めたん",
                "speakerName": "四国めたん",
                "speakerStyle": "ノーマル",
              },
            ],
          },
        ],
      }
    `);

    expect(
      parseYamlConfig(`
!pset
global_presets:
  - label: "mei"
    engine: "open_jtalk"
    htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
  - label: "四国めたん"
    engine: "voicevox"
    speakerName: "四国めたん"
    speakerStyle: "ノーマル"

---

clients:
  - command: ".test"
    token: "."
    tts: !pget
      from: global_presets
      choices: mei
      default: mei
  - command: ".test2"
    token: "."
    tts: !pget
      choices:
        - mei
        - 四国めたん
      default: 四国めたん
      from: global_presets`),
    ).toMatchInlineSnapshot(`
      {
        "clients": [
          {
            "command": ".test",
            "token": ".",
            "tts": Presets [
              {
                "default": true,
                "engine": "open_jtalk",
                "htsvoicePath": "/var/open_jtalk/voice/mei_normal.htsvoice",
                "label": "mei",
              },
            ],
          },
          {
            "command": ".test2",
            "token": ".",
            "tts": Presets [
              {
                "default": false,
                "engine": "open_jtalk",
                "htsvoicePath": "/var/open_jtalk/voice/mei_normal.htsvoice",
                "label": "mei",
              },
              {
                "default": true,
                "engine": "voicevox",
                "label": "四国めたん",
                "speakerName": "四国めたん",
                "speakerStyle": "ノーマル",
              },
            ],
          },
        ],
      }
    `);
  });

  it("can throw on invalid configs", () => {
    expect(() =>
      parseYamlConfig(`
!pset
global_presets:
  - label: "mei"
    engine: "open_jtalk"
    htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
  - label: "四国めたん"
    engine: "voicevox"
    speakerName: "四国めたん"
    speakerStyle: "ノーマル"

---

clients:
  - command: ".test"
    token: "."
    tts: !pget
  - command: ".test2"
    token: "."
    tts: !pget
      default: "四国めたん"
      from: global_presets`),
    ).toThrow();

    expect(() =>
      parseYamlConfig(`
!pset
global_presets:
  - label: "mei"
    engine: "open_jtalk"
    htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
  - label: "四国めたん"
    engine: "voicevox"
    speakerName: "四国めたん"
    speakerStyle: "ノーマル"

---

clients:
  - command: ".test"
    token: "."
    tts: !pget
      from: global_presets
  - command: ".test2"
    token: "."
    tts: !pget
      default: "四国めたん"
      presets: "invalid"`),
    ).toThrow();

    expect(() =>
      parseYamlConfig(`
!pset
global_presets: {}

---

clients:
  - command: ".test"
    token: "."
    tts: !pget
      from: global_presets
  - command: ".test2"
    token: "."
    tts: !pget
      default: "四国めたん"
      from: global_presets`),
    ).toThrow();

    expect(() =>
      parseYamlConfig(`
tts: &global_presets
  - label: "mei"
    engine: "open_jtalk"
    htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
  - label: "四国めたん"
    engine: "voicevox"
    speakerName: "四国めたん"
    speakerStyle: "ノーマル"

clients:
  - command: ".test"
    token: "."
    tts: !presets
      presets: *global_presets
  - command: ".test2"
    token: "."
    tts: !presets
      default: "四国めたん"
      presets: *global_presets`),
    ).toThrow();

    expect(() =>
      parseYamlConfig(`
!pset
global_presets:
  - label: "mei"
    engine: "open_jtalk"
    htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice"
  - label: "四国めたん"
    engine: "voicevox"
    speakerName: "四国めたん"
    speakerStyle: "ノーマル"

---

clients:
  - command: ".test"
    token: "."
    tts: !pget
      presets: global_presets
      pick:
        - mei
        - 四国めたん
  - command: ".test2"
    token: "."
    tts: !pget
      default: "四国めたん"
      presets: global_presets`),
    ).toThrow();
  });
});
