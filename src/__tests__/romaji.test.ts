import { describe, it, expect } from "vitest";
import { applyDicts } from "../dict/index.js";
import { decodeRomaji, decodeRomajiAll, hasRomaji } from "../utils/romaji.js";

describe("romaji", () => {
  it("can find romaji", () => {
    expect(hasRomaji("kikkomanなのですよ")).toBe(true);
    expect(hasRomaji("mizkanですわよ！")).toBe(false);
  });

  it("can decode romaji", () => {
    expect(decodeRomaji("sumabura")).toBe("スマブラ");
    expect(decodeRomaji("sa-barukyatto")).toBe("サーバルキャット");
  });

  it("can decode all romaji in a given text", () => {
    expect(
      decodeRomajiAll(
        "Kazuyaの立ち回りをreviewしないとPDCA cycle回せませんよ！",
      ),
    ).toBe("カズヤの立ち回りをreviewしないとPDCA cycle回せませんよ！");

    expect(
      decodeRomajiAll(
        "待ってくださいBobさん、MarukomeはマルコムXとは無関係ですよ。",
      ),
    ).toBe("待ってくださいBobさん、マルコメはマルコムXとは無関係ですよ。");
  });

  it("is compatible with dictionary", async () => {
    expect(await applyDicts("tesutodayo-", decodeRomajiAll)).toBe(
      "テストダヨー",
    );

    expect(await applyDicts("テsutoナno-", decodeRomajiAll)).toBe(
      "テストナノー",
    );
  });
});
