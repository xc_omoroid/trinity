import { beforeEach, describe, it, expect, vi } from "vitest";
import { getEnv } from "../utils/getEnv.js";

beforeEach(() => {
  vi.unstubAllEnvs();
});

describe("getEnv", () => {
  it("can get environment variable", () => {
    vi.stubEnv("TEST_ENV", "TEST_VALUE");

    const onFound = vi.fn((value: string) => value);
    const onNotFound = vi.fn(() => undefined);

    expect(
      getEnv({
        name: "TEST_ENV",
        onFound: onFound,
        onNotFound: onNotFound,
      }),
    ).toEqual("TEST_VALUE");

    expect(onFound).toBeCalledTimes(1);
    expect(onNotFound).not.toBeCalled();
  });

  it("will process the environment variable with onFound", () => {
    vi.stubEnv("TEST_ENV", "TEST_VALUE");

    const onFound = vi.fn((string: string) => string.toLowerCase());
    const onNotFound = vi.fn(() => undefined);

    expect(
      getEnv({
        name: "TEST_ENV",
        onFound: onFound,
        onNotFound: onNotFound,
      }),
    ).toEqual("test_value");

    expect(onFound).toBeCalledTimes(1);
    expect(onNotFound).not.toBeCalled();
  });

  it("runs onNotFound if the environment variable is unavailable", () => {
    const onFound = vi.fn(() => undefined);
    const onNotFound = vi.fn(() => "fallback");

    expect(
      getEnv({
        name: "TEST_ENV",
        onFound: onFound,
        onNotFound: onNotFound,
      }),
    ).toEqual("fallback");

    expect(onNotFound).toBeCalledTimes(1);
    expect(onFound).not.toBeCalled();
  });

  it("passes ENOENT to onNotFound if the environment variable is unavailable", () => {
    const onFound = vi.fn(() => undefined);
    const onNotFound = vi.fn((error: NodeJS.ErrnoException) => [
      error.code,
      error,
    ]);

    expect(
      getEnv({
        name: "TEST_ENV",
        onFound: onFound,
        onNotFound: onNotFound,
      }),
    ).toMatchInlineSnapshot(`
      [
        "ENOENT",
        [Error: $TEST_ENV does not exist!],
      ]
    `);

    expect(onNotFound).toBeCalledTimes(1);
    expect(onFound).not.toBeCalled();
  });
});
