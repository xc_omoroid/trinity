import { describe, it, expect } from "vitest";
import { Presets } from "../utils.js";

describe("presets", () => {
  it("constructs", () => {
    expect(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ] as const),
    ).toBeTruthy();
  });

  it("copies itself", () => {
    expect(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ] as const).copy(),
    ).toMatchObject(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ] as const),
    );
  });

  it("picks presets", () => {
    expect(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ] as const).pick("四国めたん", "mei"),
    ).toMatchObject(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ] as const),
    );
  });

  it("handles default", () => {
    expect(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ] as const).setDefault("ずんだもん"),
    ).toMatchObject(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
          default: false,
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
          default: true,
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
          default: false,
        },
      ] as const),
    );

    expect(
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ] as const)
        .pick("ずんだもん", "mei")
        .setDefault("mei"),
    ).toMatchObject(
      new Presets([
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
          default: false,
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
          default: true,
        },
      ] as const),
    );
  });

  it("throws error on invalid operations", () => {
    expect(() => new Presets([])).toThrow();

    const presetsWithNoElements = new Presets([
      {
        label: "四国めたん",
        engine: "voicevox",
        speakerName: "四国めたん",
        speakerStyle: "ノーマル",
      },
      {
        label: "ずんだもん",
        engine: "voicevox",
        speakerName: "ずんだもん",
        speakerStyle: "ノーマル",
      },
      {
        label: "mei",
        engine: "open_jtalk",
        htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
      },
    ]);

    presetsWithNoElements.length = 0;

    expect(() => presetsWithNoElements.copy()).toThrow();

    expect(() =>
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]).pick(),
    ).toThrow();

    expect(() =>
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]).pick("invalid"),
    ).toThrow();

    expect(() =>
      new Presets([
        {
          label: "四国めたん",
          engine: "voicevox",
          speakerName: "四国めたん",
          speakerStyle: "ノーマル",
        },
        {
          label: "ずんだもん",
          engine: "voicevox",
          speakerName: "ずんだもん",
          speakerStyle: "ノーマル",
        },
        {
          label: "mei",
          engine: "open_jtalk",
          htsvoicePath: "/var/open_jtalk/voice/mei_normal.htsvoice",
        },
      ]).setDefault("invalid"),
    ).toThrow();
  });
});
