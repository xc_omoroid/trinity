import { Routes } from "discord-api-types/v9";
import { REST } from "@discordjs/rest";
import * as Discord from "discord.js";
import { logger } from "../utils/logger.js";
import { trinityDb } from "../db/redis.js";
import { createHash } from "node:crypto";
import { type Command, getInteractions } from "./index.js";
import type { TrinityConfigParsed } from "../config/schema.js";
import objectHash from "object-hash";
import { GatewayIntentBits } from "discord.js";

const interactionHashKey = "command-deploy-digest";

type DeployCommandsOptionsBase = {
  token: string;
  applicationId: string;
  clientUserTag: string;
  skipCheck?: boolean;
};

type DeployCommandsOptions =
  | (DeployCommandsOptionsBase & {
      configureGuilds: false;
      scope: "application";
    })
  | (DeployCommandsOptionsBase & {
      configureGuilds: true;
      scope: "application" | "guild";
      guilds: string[];
    });

const deployCommands = async (options: DeployCommandsOptions) => {
  const rest = new REST({ version: "10" }).setToken(options.token);

  logger.trace("Generated REST object.");

  if (logger.isLevelEnabled("trace")) {
    logger.trace(
      {
        application: await rest.get(
          Routes.applicationCommands(options.applicationId),
        ),
        guilds: options.configureGuilds
          ? await Promise.all(
              options.guilds
                .map(async (guild) => {
                  try {
                    const response = await rest.get(
                      Routes.applicationGuildCommands(
                        options.applicationId,
                        guild,
                      ),
                    );

                    return response;
                  } catch (error: unknown) {
                    logger.warn(
                      { err: error, tag: options.clientUserTag },
                      "Failed to fetch command. You can safely ignore this warning.",
                    );

                    return null;
                  }
                })
                .filter((result) => result != null),
            )
          : null,
      },
      `Fetched commands for ${options.clientUserTag}.`,
    );
  }

  const body = (await getInteractions())
    .filter(
      (interaction): interaction is Command => interaction.type === "command",
    )
    .map((command) => command.data.toJSON());

  const dbDigestKey = interactionHashKey + "-" + options.applicationId;

  const serializedBody: string = objectHash(body, {
    algorithm: "passthrough",
    unorderedArrays: true,
    unorderedSets: true,
    unorderedObjects: true,
  });

  const bodyDigest = createHash("sha256")
    .update(
      options.token + options.applicationId + options.scope + serializedBody,
    )
    .digest("hex");

  if (
    options.skipCheck !== true &&
    (await trinityDb.get(dbDigestKey)) === bodyDigest
  ) {
    logger.info(`Slash commands for ${options.clientUserTag} are up to date.`);
    return;
  }

  if (options.scope === "guild") {
    if (options.guilds.length === 0) {
      logger.warn(`${options.clientUserTag} has no guilds to deploy.`);
      return;
    }

    logger.debug(
      `Removing application commands from ${options.clientUserTag}.`,
    );

    await rest.put(Routes.applicationCommands(options.applicationId), {
      body: [],
    });

    for (const guildId of options.guilds) {
      try {
        logger.debug(
          { guildId, body },
          `Deploying guild commands for ${options.clientUserTag}`,
        );

        await rest.put(
          Routes.applicationGuildCommands(options.applicationId, guildId),
          {
            body,
          },
        );
      } catch (error: unknown) {
        logger.warn(
          { err: error, tag: options.clientUserTag },
          "Failed to put command. You can safely ignore this warning.",
        );
      }
    }
  } else {
    if (options.configureGuilds) {
      for (const guildId of options.guilds) {
        try {
          logger.debug(
            { guildId },
            `Removing guild commands from ${options.clientUserTag}.`,
          );

          await rest.put(
            Routes.applicationGuildCommands(options.applicationId, guildId),
            {
              body: [],
            },
          );
        } catch (error: unknown) {
          logger.warn(
            { err: error, tag: options.clientUserTag },
            "Failed to put command. You can safely ignore this warning.",
          );
        }
      }
    }

    logger.debug(
      { body },
      `Deploying application commands for ${options.clientUserTag}.`,
    );

    await rest.put(Routes.applicationCommands(options.applicationId), {
      body,
    });
  }

  await trinityDb.set(dbDigestKey, bodyDigest);

  logger.info(
    `All commands deployed for the application ${options.clientUserTag} in ${options.scope} scope.`,
  );
};

export const syncApplicationCommands = async (
  config: TrinityConfigParsed,
  force = false,
) => {
  const scope =
    process.env.NODE_ENV === "development" ? "guild" : "application";

  const configureGuilds = scope === "guild" || force;

  const deployments = config.clients.map(async (clientConfig) => {
    const client = new Discord.Client({
      intents: [GatewayIntentBits.Guilds],
    });

    try {
      const token = clientConfig.token.trim();

      void client.login(token);
      await new Promise((resolve) => client.once("ready", resolve));

      if (!client.application) {
        throw new Error(`Application is not available.`);
      }

      if (!client.user) {
        throw new Error("User is not available.");
      }

      const application = client.application;

      if (configureGuilds) {
        const guilds = await client.guilds
          .fetch({ limit: 5 })
          .then((oauth2Guilds) =>
            oauth2Guilds.toJSON().map((guild) => guild.id),
          );

        logger.trace({ guilds }, `Fetched guilds for ${client.user.tag}.`);

        await deployCommands({
          scope,
          configureGuilds,
          token,
          guilds,
          applicationId: application.id,
          clientUserTag: client.user.tag,
          skipCheck: force,
        });
      } else {
        await deployCommands({
          scope,
          configureGuilds,
          token,
          applicationId: application.id,
          clientUserTag: client.user.tag,
          skipCheck: force,
        });
      }
    } finally {
      await client.destroy();
      logger.trace(
        `Client for ${
          client.user?.tag || client.application?.id || client.token
        } destroyed successfully.`,
      );
    }
  });

  const fails = (await Promise.allSettled(deployments)).reduce(
    (count, result) => {
      if (result.status === "fulfilled") {
        return count;
      }

      logger.error(
        { err: result.reason as unknown },
        "Failed to configure commands.",
      );

      return count + 1;
    },
    0,
  );

  logger.info(
    `Configuration of slash commands finished with ${fails || "no"} ${
      fails === 1 ? "fail" : "fails"
    }.`,
  );
};
