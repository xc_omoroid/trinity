import {
  ApplicationCommandType,
  BaseInteraction,
  ButtonInteraction,
  ChatInputCommandInteraction,
  Collection,
  type Interaction,
  type InteractionReplyOptions,
  InteractionType,
  StringSelectMenuInteraction,
  SlashCommandBuilder,
  type SlashCommandSubcommandsOnlyBuilder,
} from "discord.js";
import { logger } from "../utils/logger.js";
import { randomUUID } from "node:crypto";
import type { Trinity } from "../server.js";

export type Context<T extends BaseInteraction> = {
  client: InstanceType<typeof Trinity>;
  interaction: T;
};

export type CommandContext = Context<ChatInputCommandInteraction>;

export type SelectMenuContext = Context<StringSelectMenuInteraction>;

export type ButtonContext = Context<ButtonInteraction>;

export type Command = {
  type: "command";
  data: SlashCommandBuilder | SlashCommandSubcommandsOnlyBuilder;
  execute: (context: CommandContext) => Promise<void>;
};

export type SelectMenu = {
  type: "selectMenu";
  execute: (context: SelectMenuContext) => Promise<void>;
};

export type Button = {
  type: "button";
  execute: (context: ButtonContext) => Promise<void>;
};

export type MessageInteraction = Command | SelectMenu | Button;

let loaded = false;

const interactions = new Collection<string, MessageInteraction>();

export const getInteractions = async () => {
  if (loaded) {
    return interactions;
  }

  interactions.set("info", (await import("./commands/info.js")).default);
  interactions.set("dict", (await import("./commands/dict.js")).default);
  interactions.set("vc", (await import("./commands/vc.js")).default);
  interactions.set("config", (await import("./commands/config.js")).default);
  interactions.set(
    "guild_config",
    (await import("./selectMenu/guildConfig/index.js")).default,
  );
  interactions.set(
    "set_guild_voice",
    (await import("./selectMenu/setVoiceOverwrite.js")).default,
  );
  interactions.set(
    "set_user_voice",
    (await import("./selectMenu/setVoiceOverwrite.js")).default,
  );
  interactions.set(
    "config_cancel",
    (await import("./button/configCancel.js")).default,
  );
  interactions.set(
    "set_read_username",
    (await import("./selectMenu/setReadUsernameOverwrite.js")).default,
  );
  interactions.set(
    "set_notify_user_activities",
    (await import("./selectMenu/setNotifyUserActivities.js")).default,
  );
  interactions.set(
    "set_variable_reading_speed",
    (await import("./selectMenu/setVariableReadingSpeedOverwrite.js")).default,
  );

  loaded = true;

  return interactions;
};

export const executeInteraction = async ({
  client,
  interaction,
}: Context<Interaction>) => {
  try {
    if (
      interaction.type === InteractionType.ApplicationCommand &&
      interaction.commandType === ApplicationCommandType.ChatInput
    ) {
      const command = (await getInteractions()).get(interaction.commandName);

      if (!command || command.type !== "command") {
        logger.error(
          { commandName: interaction.commandName },
          "Command not found.",
        );
        return;
      }

      await command.execute({ client, interaction });
    } else if (interaction.isStringSelectMenu()) {
      const selectMenu = (await getInteractions()).get(interaction.customId);

      if (!selectMenu || selectMenu.type !== "selectMenu") {
        logger.error(
          { customId: interaction.customId },
          "Select menu not found.",
        );
        return;
      }

      await selectMenu.execute({ client, interaction });
    } else if (interaction.isButton()) {
      const button = (await getInteractions()).get(interaction.customId);

      if (!button || button.type !== "button") {
        logger.error({ customId: interaction.customId }, "Button not found.");
        return;
      }

      await button.execute({ client, interaction });
    }
  } catch (error: unknown) {
    if (
      interaction.type === InteractionType.ApplicationCommand ||
      interaction.type === InteractionType.MessageComponent
    ) {
      const ticket = randomUUID();

      logger.error({ ticket, err: error }, "Failed to run command.");
      const payload: InteractionReplyOptions = {
        content: `コマンドの実行に失敗しました。(エラーコード: ${ticket})`,
        ephemeral: true,
      };

      const reply = await interaction.fetchReply();

      if (reply) {
        await interaction.followUp(payload);
      } else {
        await interaction.reply(payload);
      }
    } else {
      logger.error({ err: error }, "Failed to run command.");
    }
  }
};
