import { EmbedBuilder } from "discord.js";

export const formatSettingChange = (before: string, after: string) =>
  new EmbedBuilder()
    .setTitle("設定を変更しました")
    .setDescription(`${before} → ${after}`);

export const formatSettingFail = (description: string) =>
  new EmbedBuilder().setTitle("設定に失敗しました").setDescription(description);

export const formatSettingCancel = () =>
  new EmbedBuilder()
    .setTitle("キャンセルしました")
    .setDescription("設定は変更されませんでした。");
