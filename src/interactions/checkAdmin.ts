import { PermissionFlagsBits } from "discord-api-types/v9";
import type { BaseInteraction } from "discord.js";

export const checkAdmin = (interaction: BaseInteraction): boolean =>
  !!interaction.guild?.members
    .resolve(interaction.user)
    ?.permissions.any([
      PermissionFlagsBits.Administrator,
      PermissionFlagsBits.ManageGuild,
    ]);
