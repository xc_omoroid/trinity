import { SlashCommandBuilder, escapeInlineCode } from "discord.js";
import {
  getCustomDictEntries,
  removeCustomDictEntry,
  setCustomDictEntry,
} from "../../dict/custom.js";
import { logger } from "../../utils/logger.js";
import { trimAll } from "../../utils/trimAll.js";
import type { Command } from "../index.js";

const command: Command = {
  type: "command",
  data: new SlashCommandBuilder()
    .setName("dict")
    .setDescription("単語辞書を操作します。")
    .addSubcommand((subcommand) =>
      subcommand
        .setName("teach")
        .setDescription("単語の読みを教えます。")
        .addStringOption((option) =>
          option
            .setName("word")
            .setDescription("覚えさせたい単語")
            .setRequired(true),
        )
        .addStringOption((option) =>
          option
            .setName("reading")
            .setDescription("覚えさせたい読み")
            .setRequired(true),
        ),
    )
    .addSubcommand((subcommand) =>
      subcommand
        .setName("forget")
        .setDescription("教えた単語の読みを忘れさせます。")
        .addStringOption((option) =>
          option
            .setName("word")
            .setDescription("忘れさせたい単語")
            .setRequired(true),
        ),
    ),
  execute: async ({ interaction }) => {
    if (!interaction.inGuild() || !interaction.guild || !interaction.channel) {
      void interaction.reply({
        content: "このコマンドはサーバー内でのみ使用できます",
        ephemeral: true,
      });

      return;
    }

    const wordRegexp = /^\S+$/u;
    const readingRegexp = /^(?:\p{scx=Hira}|\p{scx=Kana})+$/u;

    if (interaction.options.getSubcommand() === "teach") {
      const word = interaction.options.getString("word");
      const reading = interaction.options.getString("reading");

      if (!word || !wordRegexp.test(word)) {
        void interaction.reply(
          trimAll(`
            単語の形式が不正です。
            /dict teach \`word: 不知火\` \`reading: しらぬい\` のように登録するキーワードを指定してください。
          `),
        );
        return;
      }

      if (!reading || !readingRegexp.test(reading)) {
        void interaction.reply(
          trimAll(`
            読みの形式が不正です。
            /dict teach \`word: 五百蔵\` \`reading: いおろい\` のように登録するキーワードを指定してください。
          `),
        );
        return;
      }

      logger.trace("Adding word-reading record.");

      void setCustomDictEntry(interaction.guild.id, word, reading);

      void interaction.reply(
        `\`${escapeInlineCode(word)}\` は \`${escapeInlineCode(
          reading,
        )}\` を登録しました。`,
      );
      return;
    }

    if (interaction.options.getSubcommand() === "forget") {
      const word = interaction.options.getString("word");

      if (!word || !wordRegexp.test(word)) {
        void interaction.reply(
          trimAll(`
            単語の形式が不正です。
            /dict forget \`word: 小此木\` のように忘れさせたいキーワードを指定してください。
          `),
        );
        return;
      }

      const dictEntries = await getCustomDictEntries(interaction.guild.id);
      const found = dictEntries.find((dict) => dict[0] === word);

      if (!found) {
        const scores = dictEntries
          .map((entry): [number, [string, string]] => {
            let score = -Infinity;
            if (entry[0].startsWith(word)) {
              score = -entry[0].length;
            } else if (word.startsWith(entry[0])) {
              score = -word.length;
            }
            return [score, entry];
          })
          .sort((aEntry, bEntry) => bEntry[0] - aEntry[0])
          .filter((entry) => Number.isFinite(entry[0]));

        logger.trace({ scores }, "Scored dictionary entry distances.");

        if (scores[0] != null) {
          void interaction.reply(
            trimAll(`
                \`${escapeInlineCode(word)}\` は登録されていません。
                もしかして: \`${escapeInlineCode(scores[0][1][0])}\`
              `),
          );
          return;
        }

        void interaction.reply(
          `\`${escapeInlineCode(word)}\` は登録されていません。`,
        );
        return;
      }

      const [from, to] = found;

      void removeCustomDictEntry(interaction.guild.id, from);

      void interaction.reply(
        `\`${escapeInlineCode(from)}\` は \`${escapeInlineCode(
          to,
        )}\` を忘れました。`,
      );
    }
  },
};

export default command;
