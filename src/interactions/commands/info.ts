import { loadavg as osLoadavg } from "node:os";
import { Trinity } from "../../server.js";
import { tinyFormat } from "../../utils/tinyDate.js";
import { trimAll } from "../../utils/trimAll.js";
import type { Command } from "../index.js";
import {
  Team,
  User,
  SlashCommandBuilder,
  escapeInlineCode,
  Status,
} from "discord.js";
import { getVersion } from "../../utils.js";
import columnify from "columnify";

const command: Command = {
  type: "command",
  data: new SlashCommandBuilder()
    .setName("info")
    .setDescription("BOTの情報を表示します。"),
  execute: async ({ client, interaction }) => {
    const start = Date.now();

    await interaction.deferReply({
      ephemeral: true,
    });

    const ping = Date.now() - start;

    const loadavg = osLoadavg()
      .map((load) => load.toFixed(2))
      .join(" ");

    const botOwners = [];

    const application = await client.client.application?.fetch();

    if (application?.owner instanceof Team) {
      botOwners.push(application.owner.members.keys());
    } else if (application?.owner instanceof User) {
      botOwners.push(application.owner.id);
    }

    const isOwner = botOwners.includes(interaction.user.id);

    const currentGuild = interaction.guildId;

    const allClients = [...Trinity.set.values()].filter(
      (unknown): unknown is InstanceType<typeof Trinity> =>
        unknown instanceof Trinity,
    );

    let pickedClients: InstanceType<typeof Trinity>[];

    if (isOwner) {
      pickedClients = allClients;
    } else if (currentGuild === null) {
      pickedClients = [client];
    } else {
      pickedClients = allClients.filter(
        (trinity) => trinity.client.guilds.resolve(currentGuild) !== null,
      );
    }

    const header = [`ping ${ping}ms`];

    if (isOwner) {
      header.push(`load averages: ${loadavg}`);
    }

    const status = await Promise.all(
      pickedClients.map(async (trinity) => ({
        INSTANCE: trinity.client.user?.username ?? trinity.bang,
        STATUS: Status[trinity.client.ws.status] ?? "Unknown",
        UPTIME: tinyFormat(trinity.client.uptime || 0),
        VCSIZE: `${(await trinity.ttsManager.getLengths()).instanceLength}`,
      })),
    );

    void interaction.editReply({
      content: trimAll(`
        Status of \`Trinity@${escapeInlineCode(await getVersion())}\`
        \`\`\`plain
        ${header.join(", ")}
        ${columnify(status)}
        \`\`\`

        The source code and licenses for Trinity can be found at: https://gitlab.com/xc_omoroid/trinity/
      `),
    });
  },
};

export default command;
