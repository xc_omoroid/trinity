import { getVoiceConnection, VoiceConnectionStatus } from "@discordjs/voice";
import { ChannelType, escapeInlineCode, SlashCommandBuilder } from "discord.js";
import { formatSystemMessage } from "../../utils/formatMessage.js";
import { logger } from "../../utils/logger.js";
import { trimAll } from "../../utils/trimAll.js";
import type { Command } from "../index.js";

const command: Command = {
  type: "command",
  data: new SlashCommandBuilder()
    .setName("vc")
    .setDescription("読み上げを操作します。")
    .addSubcommand((subcommand) =>
      subcommand.setName("join").setDescription("読み上げを開始します。"),
    )
    .addSubcommand((subcommand) =>
      subcommand.setName("bye").setDescription("読み上げを終了します。"),
    ),
  execute: async ({ client, interaction }) => {
    if (!interaction.inGuild() || !interaction.guild || !interaction.channel) {
      void interaction.reply({
        content: "読み上げ機能はサーバー内でのみ使用できます",
        ephemeral: true,
      });

      return;
    }

    if (interaction.options.getSubcommand() === "join") {
      const caller = await interaction.guild.members.fetch(interaction.user);

      if (!caller) {
        logger.error(
          {
            id: interaction.user.id,
            tag: interaction.user.tag,
            bot: interaction.user.bot,
          },
          "Failed to fetch user.",
        );

        throw new Error("Failed to fetch user.");
      }

      const voiceChannel = caller.voice.channel;

      if (!voiceChannel) {
        void interaction.reply({
          content:
            "このコマンドを実行するには、いずれかのボイスチャンネルに所属する必要があります。",
          ephemeral: true,
        });

        return;
      }

      const voiceConnectionStatus = getVoiceConnection(
        interaction.guild.id,
        client.tokenHash,
      )?.state.status;

      if (
        voiceConnectionStatus === VoiceConnectionStatus.Disconnected ||
        voiceConnectionStatus === VoiceConnectionStatus.Destroyed
      ) {
        logger.info("Cleaning up voice connection.");
        await client.ttsManager.remove(interaction.guild.id);
      }

      const currentTTSData = await client.ttsManager.get(interaction.guild.id);

      if (currentTTSData) {
        if (
          interaction.channel.id === currentTTSData.ttsState.textChannelId &&
          voiceChannel.id === currentTTSData.ttsState.voiceChannelId
        ) {
          void interaction.reply({
            content: `${
              client.client.user?.username || "このBOT"
            }は既にこのチャンネルに登録されています。`,
            ephemeral: true,
          });

          return;
        }

        let configuredChannelName = "別のチャンネル";

        if (interaction.channelId !== currentTTSData.ttsState.textChannelId) {
          const configuredTextChannel = await interaction.guild.channels.fetch(
            currentTTSData.ttsState.textChannelId,
          );

          if (
            configuredTextChannel != null &&
            configuredTextChannel.type === ChannelType.GuildText
          ) {
            configuredChannelName = `テキストチャンネル \`${escapeInlineCode(
              configuredTextChannel.name,
            )}\` `;
          } else if (
            configuredTextChannel != null &&
            configuredTextChannel.type === ChannelType.GuildVoice
          ) {
            configuredChannelName = `ボイスチャンネル \`${escapeInlineCode(
              configuredTextChannel.name,
            )}\` 内のテキストチャット`;
          } else {
            configuredChannelName = "別のテキストチャンネル";
          }
        } else if (voiceChannel.id !== currentTTSData.ttsState.voiceChannelId) {
          const configuredVoiceChannel = await interaction.guild.channels.fetch(
            currentTTSData.ttsState.voiceChannelId,
          );

          if (
            configuredVoiceChannel != null &&
            configuredVoiceChannel.type === ChannelType.GuildVoice
          ) {
            configuredChannelName = `ボイスチャンネル \`${escapeInlineCode(
              configuredVoiceChannel.name,
            )}\` `;
          } else {
            configuredChannelName = "別のボイスチャンネル";
          }
        }

        void interaction.reply({
          content: trimAll(`
              ${
                client.client.user?.username ?? "このBOT"
              }は${configuredChannelName}に登録されています。
              このチャンネルで使いたい場合は \`/vc bye\` で切断してから再接続してください。
            `),
          ephemeral: true,
        });

        return;
      }

      const deferredReply = await interaction.deferReply({
        fetchReply: true,
      });

      const ttsData = await client.ttsManager.connect(
        deferredReply.id,
        interaction.channel.id,
        voiceChannel.id,
      );

      if (!ttsData) {
        logger.error(
          {
            interactionChannelId: interaction.channel.id,
            voiceChannelId: voiceChannel.id,
          },
          "Failed to initialize TTS.",
        );
        throw new Error("Failed to initialize TTS.");
      }

      await interaction.editReply({
        embeds: [formatSystemMessage("ボイスチャットに接続しました。")],
      });

      await client.ttsManager.updateState(interaction.guild.id, {
        lastAddedMessageId: deferredReply.id,
        lastAddedUserId: deferredReply.author.id,
        lastPlayedMessageId: deferredReply.id,
        lastPlayedUserId: deferredReply.author.id,
      });

      logger.info(
        {
          channel: voiceChannel.name,
          lengths: await client.ttsManager.getLengths(),
        },
        "Registered TTS instance.",
      );
    }

    if (interaction.options.getSubcommand() === "bye") {
      const ttsManagerData = await client.ttsManager.get(interaction.guild.id);

      if (
        ttsManagerData &&
        ttsManagerData.ttsState.textChannelId !== interaction.channel.id
      ) {
        logger.debug(
          {
            channelA: ttsManagerData.ttsState.textChannelId,
            channelB: interaction.channel.id,
          },
          "Called bye from unrelated voice channel",
        );

        void interaction.reply({
          content: `${
            client.client.user?.username || "このBOT"
          }はこのチャンネルに未接続です。`,
          ephemeral: true,
        });

        return;
      }

      if (getVoiceConnection(interaction.guild.id, client.tokenHash)) {
        await client.ttsManager.disconnect(interaction.guild.id);

        void interaction.reply({
          embeds: [formatSystemMessage("ボイスチャットから切断しました。")],
        });
      } else {
        logger.debug(
          { guild: interaction.guild.name },
          "Already disconnected.",
        );

        void interaction.reply({
          content: "切断済みです。",
          ephemeral: true,
        });
      }
    }
  },
};

export default command;
