import {
  ActionRowBuilder,
  ButtonBuilder,
  StringSelectMenuBuilder,
  SlashCommandBuilder,
} from "discord.js";
import { cancelButton } from "../button/configCancel.js";
import { checkAdmin } from "../checkAdmin.js";
import type { Command } from "../index.js";

const command: Command = {
  type: "command",
  data: new SlashCommandBuilder()
    .setName("config")
    .setDescription("設定を開きます。"),
  execute: async ({ interaction }) => {
    const selectMenu = new StringSelectMenuBuilder()
      .setCustomId("guild_config")
      .setPlaceholder("選択…");

    if (checkAdmin(interaction)) {
      selectMenu.addOptions([
        {
          label: "ボイス設定 (サーバー)",
          description: "サーバー内の読み上げボイスの設定を変更します。",
          value: "guild_voice",
        },
        {
          label: "ボイス設定 (ユーザー)",
          description: "あなたの読み上げボイスの設定を変更します。",
          value: "user_voice",
        },
        {
          label: "名前の読み上げ",
          description: "発言時にユーザー名を読み上げるか設定します。",
          value: "read_username",
        },
        {
          label: "入退室通知",
          description: "メンバーの接続と切断を通知するか設定します。",
          value: "notify_user_activities",
        },
        {
          label: "読み上げの加速",
          description: "発言が重なった時、読み上げを速くするか設定します。",
          value: "variable_reading_speed",
        },
      ]);
    } else {
      selectMenu.addOptions({
        label: "ボイス設定",
        description: "読み上げボイスの設定を変更します。",
        value: "user_voice",
      });
    }

    const selectMenuRow =
      new ActionRowBuilder<StringSelectMenuBuilder>().addComponents([
        selectMenu,
      ]);

    const cancelButtonRow = new ActionRowBuilder<ButtonBuilder>().addComponents(
      [cancelButton()],
    );

    await interaction.reply({
      content: "変更したい項目を選択してください。",
      components: [selectMenuRow, cancelButtonRow],
      ephemeral: true,
    });
  },
};

export default command;
