import { ButtonBuilder, ButtonStyle } from "discord.js";
import { formatSettingCancel } from "../formatSettingMessage.js";
import type { Button } from "../index.js";

export const cancelButton = () =>
  new ButtonBuilder()
    .setCustomId("config_cancel")
    .setStyle(ButtonStyle.Secondary)
    .setLabel("キャンセル");

const followup: Button = {
  type: "button",
  execute: async ({ interaction }) => {
    await interaction.update({
      embeds: [formatSettingCancel()],
      components: [],
    });
  },
};

export default followup;
