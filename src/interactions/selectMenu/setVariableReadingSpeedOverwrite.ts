import {
  getGuildOverwrites,
  setGuildOverwrites,
} from "../../db/localOverwrites.js";
import {
  formatSettingChange,
  formatSettingFail,
} from "../formatSettingMessage.js";
import type { SelectMenu } from "../index.js";
import { formatVariableReadingSpeed } from "./guildConfig/variableReadingSpeed.js";

const followup: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error(
        "Variable reading speed overwrites are only configurable in Guilds.",
      );
    }

    const guildId = interaction.guild.id;
    const value = interaction.values[0] === "true";

    if (value == null) {
      await interaction.update({
        embeds: [formatSettingFail("設定が選択されてません。")],
        components: [],
      });

      return;
    }

    const before = (
      await getGuildOverwrites({
        tokenHash: client.tokenHash,
        guildId: guildId,
      })
    ).variableReadingSpeed;

    await setGuildOverwrites({
      tokenHash: client.tokenHash,
      guildId: guildId,
      value: {
        variableReadingSpeed: value,
      },
    });

    await interaction.update({
      embeds: [
        formatSettingChange(
          formatVariableReadingSpeed(before),
          formatVariableReadingSpeed(value),
        ),
      ],
      components: [],
    });
  },
};

export default followup;
