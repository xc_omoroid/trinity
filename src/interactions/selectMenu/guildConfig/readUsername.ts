import {
  ActionRowBuilder,
  ButtonBuilder,
  StringSelectMenuBuilder,
} from "discord.js";
import { getGuildOverwrites } from "../../../db/localOverwrites.js";
import { cancelButton } from "../../button/configCancel.js";
import type { SelectMenu } from "../../index.js";

export const formatReadUsername = (readUsername: boolean) =>
  `読み上げ${readUsername ? "る" : "ない"}`;

const followup: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error(
        "Username reading overwrites are only configurable in Guilds.",
      );
    }

    const guildId = interaction.guild.id;

    const current = (
      await getGuildOverwrites({
        tokenHash: client.tokenHash,
        guildId,
      })
    ).readUsername;

    const readUsernameOptions: [string, string, boolean][] = [
      ...[true, false].map((readUsername): [string, string, boolean] => [
        formatReadUsername(readUsername),
        readUsername ? "true" : "false",
        current == readUsername,
      ]),
    ];

    const selectMenuRow =
      new ActionRowBuilder<StringSelectMenuBuilder>().setComponents([
        new StringSelectMenuBuilder()
          .setCustomId("set_read_username")
          .setOptions(
            readUsernameOptions.map(([label, selectMenuValue, isSelected]) => ({
              label: label,
              value: selectMenuValue,
              default: isSelected,
            })),
          ),
      ]);

    const cancelButtonRow = new ActionRowBuilder<ButtonBuilder>().setComponents(
      [cancelButton()],
    );

    await interaction.update({
      content: "ユーザー名を読み上げますか？",
      components: [selectMenuRow, cancelButtonRow],
    });
  },
};

export default followup;
