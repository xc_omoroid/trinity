import {
  ActionRowBuilder,
  ButtonBuilder,
  StringSelectMenuBuilder,
} from "discord.js";
import { getGuildOverwrites } from "../../../db/localOverwrites.js";
import { cancelButton } from "../../button/configCancel.js";
import type { SelectMenu } from "../../index.js";

export const formatNotifyUserActivities = (readUsername: boolean) =>
  `通知${readUsername ? "する" : "しない"}`;

const followup: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error(
        "User activity notification overwrites are only configurable in Guilds.",
      );
    }

    const guildId = interaction.guild.id;

    const current = (
      await getGuildOverwrites({
        tokenHash: client.tokenHash,
        guildId,
      })
    ).notifyUserActivities;

    const readUsernameOptions: [string, string, boolean][] = [
      ...[true, false].map((notify): [string, string, boolean] => [
        formatNotifyUserActivities(notify),
        notify ? "true" : "false",
        current == notify,
      ]),
    ];

    const selectMenuRow =
      new ActionRowBuilder<StringSelectMenuBuilder>().setComponents([
        new StringSelectMenuBuilder()
          .setCustomId("set_notify_user_activities")
          .setOptions(
            readUsernameOptions.map(([label, selectMenuValue, isSelected]) => ({
              label: label,
              value: selectMenuValue,
              default: isSelected,
            })),
          ),
      ]);

    const cancelButtonRow = new ActionRowBuilder<ButtonBuilder>().setComponents(
      [cancelButton()],
    );

    await interaction.update({
      content: "メンバーの入退室を通知しますか？",
      components: [selectMenuRow, cancelButtonRow],
    });
  },
};

export default followup;
