import {
  ActionRowBuilder,
  ButtonBuilder,
  StringSelectMenuBuilder,
} from "discord.js";
import { getGuildOverwrites } from "../../../db/localOverwrites.js";
import { cancelButton } from "../../button/configCancel.js";
import type { SelectMenu } from "../../index.js";

export const formatVariableReadingSpeed = (readUsername: boolean) =>
  `速く${readUsername ? "する" : "しない"}`;

const followup: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error(
        "Variable reading speed overwrites are only configurable in Guilds.",
      );
    }

    const guildId = interaction.guild.id;

    const current = (
      await getGuildOverwrites({
        tokenHash: client.tokenHash,
        guildId,
      })
    ).variableReadingSpeed;

    const readingSpeedOptions: [string, string, boolean][] = [
      ...[true, false].map((variableSpeed): [string, string, boolean] => [
        formatVariableReadingSpeed(variableSpeed),
        variableSpeed ? "true" : "false",
        current == variableSpeed,
      ]),
    ];

    const selectMenuRow =
      new ActionRowBuilder<StringSelectMenuBuilder>().setComponents([
        new StringSelectMenuBuilder()
          .setCustomId("set_variable_reading_speed")
          .setOptions(
            readingSpeedOptions.map(([label, selectMenuValue, isSelected]) => ({
              label: label,
              value: selectMenuValue,
              default: isSelected,
            })),
          ),
      ]);

    const cancelButtonRow = new ActionRowBuilder<ButtonBuilder>().setComponents(
      [cancelButton()],
    );

    await interaction.update({
      content: "発言が重なった時、読み上げを速くしますか？",
      components: [selectMenuRow, cancelButtonRow],
    });
  },
};

export default followup;
