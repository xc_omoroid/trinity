import { Collection } from "discord.js";
import { logger } from "../../../utils/logger.js";
import type { SelectMenu } from "../../index.js";

let loaded = false;

const prompts = new Collection<string, SelectMenu>();

export const getPrompts = async () => {
  if (loaded) {
    return prompts;
  }

  prompts.set("guild_voice", (await import("./voiceOverwrite.js")).default);
  prompts.set("user_voice", (await import("./voiceOverwrite.js")).default);
  prompts.set("read_username", (await import("./readUsername.js")).default);
  prompts.set(
    "notify_user_activities",
    (await import("./notifyUserActivities.js")).default,
  );
  prompts.set(
    "variable_reading_speed",
    (await import("./variableReadingSpeed.js")).default,
  );

  loaded = true;

  return prompts;
};

const selectMenu: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error("Guild config is only callable in Guilds.");
    }

    const promptName = interaction.values[0];

    if (promptName == null) {
      throw new Error("Value seems to be empty.");
    }

    const prompt = (await getPrompts()).get(promptName);

    if (!prompt) {
      logger.error({ promptName }, "Prompt not found.");
      return;
    }

    await prompt.execute({ client, interaction });
  },
};

export default selectMenu;
