import {
  ActionRowBuilder,
  ButtonBuilder,
  StringSelectMenuBuilder,
} from "discord.js";
import { ttsPresets } from "../../../config/schema.js";
import { getVoiceOverwrites, listPresets } from "../../../tts/manager.js";
import { cancelButton } from "../../button/configCancel.js";
import { formatSettingFail } from "../../formatSettingMessage.js";
import type { SelectMenu } from "../../index.js";

const followup: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error("Voice overwrites are only configurable in Guilds.");
    }

    const guildId = interaction.guild.id;
    const userId = interaction.user.id;
    const value = interaction.values[0];

    if (value == null) {
      throw new Error("Voice config name empty.");
    }

    const presets = ttsPresets.safeParse(client.ttsConfig);

    if (!presets.success) {
      await interaction.update({
        embeds: [
          formatSettingFail(
            "このサーバー、または BOT のインスタンスはプリセットをサポートしていません。",
          ),
        ],
        components: [],
      });
      return;
    }

    let defaultVoice: string;
    let currentVoice: string;
    let configured: boolean;

    if (value === "guild_voice") {
      ({ defaultVoice, currentVoice, configured } = await getVoiceOverwrites({
        tokenHash: client.tokenHash,
        presets: presets.data,
        guildId,
      }));
    } else if (value === "user_voice") {
      ({ defaultVoice, currentVoice, configured } = await getVoiceOverwrites({
        tokenHash: client.tokenHash,
        presets: presets.data,
        guildId,
        userId,
      }));
    } else {
      throw Error("Got invalid value.");
    }

    const presetOptions: [string, string, boolean][] = [
      [`おまかせ (${defaultVoice})`, "reset", !configured],
      ...listPresets(presets.data).map((name): [string, string, boolean] => [
        name,
        `set_${name}`,
        configured && currentVoice === name,
      ]),
    ];

    const selectMenuRow =
      new ActionRowBuilder<StringSelectMenuBuilder>().setComponents([
        new StringSelectMenuBuilder().setCustomId(`set_${value}`).setOptions(
          presetOptions.map(([name, selectMenuValue, isSelected]) => ({
            label: name,
            value: selectMenuValue,
            default: isSelected,
          })),
        ),
      ]);

    const cancelButtonRow = new ActionRowBuilder<ButtonBuilder>().setComponents(
      [cancelButton()],
    );

    await interaction.update({
      content: `${
        value === "guild_voice" ? "サーバー内" : "あなた"
      }の文章を読み上げるボイスを選択してください。`,
      components: [selectMenuRow, cancelButtonRow],
    });
  },
};

export default followup;
