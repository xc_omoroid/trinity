import {
  getGuildOverwrites,
  setGuildOverwrites,
} from "../../db/localOverwrites.js";
import {
  formatSettingChange,
  formatSettingFail,
} from "../formatSettingMessage.js";
import type { SelectMenu } from "../index.js";
import { formatNotifyUserActivities } from "./guildConfig/notifyUserActivities.js";

const followup: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error(
        "User activity notification overwrites are only configurable in Guilds.",
      );
    }

    const guildId = interaction.guild.id;
    const value = interaction.values[0] === "true";

    if (value == null) {
      await interaction.update({
        embeds: [formatSettingFail("設定が選択されてません。")],
        components: [],
      });

      return;
    }

    const before = (
      await getGuildOverwrites({
        tokenHash: client.tokenHash,
        guildId: guildId,
      })
    ).notifyUserActivities;

    await setGuildOverwrites({
      tokenHash: client.tokenHash,
      guildId: guildId,
      value: {
        notifyUserActivities: value,
      },
    });

    await interaction.update({
      embeds: [
        formatSettingChange(
          formatNotifyUserActivities(before),
          formatNotifyUserActivities(value),
        ),
      ],
      components: [],
    });
  },
};

export default followup;
