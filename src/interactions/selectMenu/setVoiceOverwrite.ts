import { ttsPresets } from "../../config/schema.js";
import {
  setGuildOverwrites,
  setUserOverwrites,
} from "../../db/localOverwrites.js";
import { iterateOverClients } from "../../db/ttsConfigHashCache.js";
import { getVoiceOverwrites, validatePresetName } from "../../tts/manager.js";
import {
  formatSettingChange,
  formatSettingFail,
} from "../formatSettingMessage.js";
import type { SelectMenu } from "../index.js";

const followup: SelectMenu = {
  type: "selectMenu",
  execute: async ({ client, interaction }) => {
    if (interaction.guild == null) {
      throw new Error("Voice overwrites are only configurable in Guilds.");
    }

    const guildId = interaction.guild.id;
    const userId = interaction.user.id;
    const value = interaction.values[0];

    if (value == null) {
      await interaction.update({
        embeds: [formatSettingFail("プリセットが選択されてません。")],
        components: [],
      });

      return;
    }

    const presets = ttsPresets.safeParse(client.ttsConfig);

    if (!presets.success) {
      await interaction.update({
        embeds: [
          formatSettingFail(
            "このサーバー、または BOT のインスタンスはプリセットをサポートしていません。",
          ),
        ],
        components: [],
      });
      return;
    }

    const targetVoice = value === "reset" ? null : value.slice("set_".length);

    if (
      targetVoice !== null &&
      !validatePresetName(targetVoice, presets.data)
    ) {
      await interaction.update({
        embeds: [formatSettingFail("プリセット名が有効ではありません。")],
        components: [],
      });

      return;
    }

    let configured: boolean;
    let defaultVoice: string;
    let currentVoice: string;

    if (interaction.customId === "set_guild_voice") {
      ({ configured, defaultVoice, currentVoice } = await getVoiceOverwrites({
        tokenHash: client.tokenHash,
        presets: presets.data,
        guildId,
      }));

      await iterateOverClients(client.ttsConfigHash, async (tokenHash) =>
        setGuildOverwrites({
          tokenHash,
          guildId,
          value: { voicePreset: targetVoice },
        }),
      );
    } else if (interaction.customId === "set_user_voice") {
      ({ configured, defaultVoice, currentVoice } = await getVoiceOverwrites({
        tokenHash: client.tokenHash,
        presets: presets.data,
        guildId,
        userId,
      }));

      await iterateOverClients(client.ttsConfigHash, async (tokenHash) =>
        setUserOverwrites({
          tokenHash,
          guildId,
          userId,
          value: { voicePreset: targetVoice },
        }),
      );
    } else {
      throw new Error("Invalid custom id.");
    }

    const omakaseVoice = `おまかせ (${defaultVoice})`;

    await interaction.update({
      embeds: [
        formatSettingChange(
          configured ? currentVoice : omakaseVoice,
          targetVoice ?? omakaseVoice,
        ),
      ],
      components: [],
    });
  },
};

export default followup;
