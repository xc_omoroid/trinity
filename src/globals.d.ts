/// <reference types="node" />

import type { Level } from "pino";

interface IProcessEnv {
  readonly NODE_ENV?: "development" | "production" | "test";
  readonly LOG_LEVEL?: Level;
  readonly DIC_PATH?: string;
  readonly OPEN_JTALK_PATH?: string;
  readonly REDIS_ENDPOINT?: string;
  readonly REDIS_PASSWORD?: string;
  readonly VOICEVOX_ENDPOINT?: string;
  readonly POSTGRES_ENDPOINT?: string;
  readonly POSTGRES_PASSWORD?: string;
}

declare global {
  declare namespace NodeJS {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface ProcessEnv extends IProcessEnv {}
  }
}

export {};
