import type { PartialDeep } from "type-fest";
import { z } from "zod";
import { logger } from "../utils/logger.js";
import { trinityDb } from "./redis.js";
import { safeJsonParse } from "../utils/safeJson.js";
import { withFallback } from "../utils/withFallback.js";
import { resetTo } from "../utils/zz.js";

const getGuildOverwritesHash = (
  // eslint-disable-next-line @typescript-eslint/ban-types
  tokenHash: (string & {}) | "*",
  guildId: string,
) => `guild-overwrites-${tokenHash}-${guildId}`;

export const userOverwritesSchema = z.object({
  voicePreset: z.string().min(1).nullish().or(resetTo(null)),
});

export const scopedGuildOverwritesSchema = userOverwritesSchema;

export const sharedGuildOverwritesSchema = z.object({
  prioritySpeaker: z.boolean().or(resetTo(false)),
  readUsername: z.boolean().or(resetTo(true)),
  notifyUserActivities: z.boolean().or(resetTo(false)),
  variableReadingSpeed: z.boolean().or(resetTo(true)),
});

type SharedGuildContext = {
  guildId: string;
};

type ScopedGuildContext = SharedGuildContext & {
  tokenHash: string;
};

export type GuildContext = ScopedGuildContext;

export type UserContext = GuildContext & {
  userId: string;
};

type ValueContext<T> = {
  value: PartialDeep<T>;
};

export type GuildValueContext<T> = GuildContext & ValueContext<T>;

export type UserValueContext<T> = UserContext & ValueContext<T>;

type SharedGuildOverwrites = z.output<typeof sharedGuildOverwritesSchema>;

type ScopedGuildOverwrites = z.output<typeof scopedGuildOverwritesSchema>;

export type GuildOverwrites = SharedGuildOverwrites & ScopedGuildOverwrites;

export type UserOverwrites = z.output<typeof userOverwritesSchema>;

export const getGuildOverwrites = async ({
  tokenHash,
  guildId,
}: GuildContext): Promise<GuildOverwrites> => {
  const sharedGuildOverwrites = sharedGuildOverwritesSchema.parse(
    safeJsonParse(
      await trinityDb.hGet(getGuildOverwritesHash("*", guildId), "*"),
    ) ?? {},
  );

  const scopedGuildOverwrites = scopedGuildOverwritesSchema.parse(
    safeJsonParse(
      await trinityDb.hGet(getGuildOverwritesHash(tokenHash, guildId), "*"),
    ) ?? {},
  );

  const guildOverwrites: GuildOverwrites = {
    ...sharedGuildOverwrites,
    ...scopedGuildOverwrites,
  };

  logger.trace(
    { tokenHash, sharedGuildOverwrites, scopedGuildOverwrites },
    "Loaded guild overwrites.",
  );

  return guildOverwrites;
};

export const setGuildOverwrites = async ({
  tokenHash,
  guildId,
  value,
}: GuildValueContext<GuildOverwrites>) => {
  const previous = await getGuildOverwrites({ tokenHash, guildId });

  const merged: GuildOverwrites = {
    ...previous,
    ...value,
  };

  const sharedGuildOverwrites = sharedGuildOverwritesSchema.safeParse(merged);

  if (sharedGuildOverwrites.success) {
    logger.trace(
      { tokenHash, sharedGuildOverwrites: sharedGuildOverwrites.data },
      "Setting shared guild overwrites.",
    );

    await trinityDb.hSet(
      getGuildOverwritesHash("*", guildId),
      "*",
      JSON.stringify(sharedGuildOverwrites.data),
    );
  }

  const scopedGuildOverwrites = scopedGuildOverwritesSchema.safeParse(merged);

  if (scopedGuildOverwrites.success) {
    logger.trace(
      { tokenHash, scopedGuildOverwrites: scopedGuildOverwrites.data },
      "Setting scoped guild overwrites.",
    );

    await trinityDb.hSet(
      getGuildOverwritesHash(tokenHash, guildId),
      "*",
      JSON.stringify(scopedGuildOverwrites.data),
    );
  }

  if (sharedGuildOverwrites.success && scopedGuildOverwrites.success) {
    // Only for error logging
    z.union([sharedGuildOverwritesSchema, scopedGuildOverwritesSchema]).parse(
      merged,
    );
  }
};

export const getUserOverwrites = async ({
  tokenHash,
  guildId,
  userId,
}: UserContext): Promise<UserOverwrites> => {
  const userOverwrites = userOverwritesSchema.parse(
    safeJsonParse(
      await trinityDb.hGet(getGuildOverwritesHash(tokenHash, guildId), userId),
    ) ?? {},
  );

  const guildOverwrites = await getGuildOverwrites({ tokenHash, guildId });

  const merged = withFallback(userOverwrites, guildOverwrites);

  logger.trace(
    { tokenHash, merged, userOverwrites, guildOverwrites },
    "Loaded user overwrites.",
  );

  return userOverwritesSchema.parse(merged);
};

export const setUserOverwrites = async ({
  tokenHash,
  guildId,
  userId,
  value,
}: UserValueContext<UserOverwrites>) => {
  const previous = await getUserOverwrites({ tokenHash, guildId, userId });

  const merged: UserOverwrites = {
    ...previous,
    ...value,
  };

  logger.trace({ tokenHash, merged }, "Setting user overwrites.");

  await trinityDb.hSet(
    getGuildOverwritesHash(tokenHash, guildId),
    userId,
    JSON.stringify(merged),
  );
};
