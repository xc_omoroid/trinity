import { createClient } from "redis";
import { logger } from "../utils/logger.js";

export const trinityDb = createClient({
  url: process.env.REDIS_ENDPOINT,
});

export const openDb = async () => {
  await trinityDb.connect();

  if (process.env.REDIS_PASSWORD) {
    await trinityDb.auth({
      password: process.env.REDIS_PASSWORD,
    });
  }

  logger.info("Connected Trinity to Redis server.");
};

export const closeDb = async () => {
  await trinityDb.disconnect();
  logger.info("Disconnected Trinity from Redis server.");
};
