import { Collection } from "discord.js";
import { logger } from "../utils/logger.js";
import objectHash from "object-hash";
import { createHash } from "node:crypto";
import type { Presets } from "../utils.js";
import type { TTSConfig } from "../config/schema.js";

const ttsConfigHashCache = new Collection<string, string[]>();

export const getTTSConfigCache = (ttsConfigHash: string): string[] => {
  const cache = ttsConfigHashCache.get(ttsConfigHash) ?? [];
  logger.trace({ ttsConfigCache: cache });
  return cache;
};

export const setTTSConfigCache = (ttsConfigHash: string, tokenHash: string) => {
  const tokenHashes = ttsConfigHashCache.get(ttsConfigHash);

  return ttsConfigHashCache.set(
    ttsConfigHash,
    tokenHashes ? [...tokenHashes, tokenHash] : [tokenHash],
  );
};

export const hashFromTTSConfig = (
  ttsConfig: TTSConfig | InstanceType<typeof Presets>,
) => {
  const serializedTTSConfig = objectHash(ttsConfig, {
    algorithm: "passthrough",
    replacer: (value: unknown) => {
      if (value == null || value === 0) {
        return null;
      }

      if (Array.isArray(value)) {
        return Array.from(value as unknown[]);
      }

      return value;
    },
    respectType: false,
    unorderedArrays: true,
    unorderedSets: true,
    unorderedObjects: true,
    excludeKeys: (key) => key === "default",
  });

  return createHash("sha256").update(serializedTTSConfig).digest("hex");
};

export const iterateOverClients = async (
  ttsConfigHash: string,
  callback: (tokenHash: string) => void,
) => {
  const tokenHashes = getTTSConfigCache(ttsConfigHash);

  const results = await Promise.allSettled(
    tokenHashes.map((tokenHash) => callback(tokenHash)),
  );

  results.forEach((result) => {
    if (result.status === "rejected") {
      logger.error({ err: result.reason });
    }
  });

  return results;
};
