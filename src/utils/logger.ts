import { pino } from "pino";

const levels = ["fatal", "error", "warn", "info", "debug", "trace", "silent"];

const isLevelValid = (level?: string): level is pino.LevelWithSilent =>
  !!level && levels.some((validLevel) => validLevel === level);

const getLevel = (): pino.LevelWithSilent => {
  if (process.env.NODE_ENV === "test") {
    return "silent";
  }

  const envLevel = process.env.LOG_LEVEL;

  if (isLevelValid(envLevel)) {
    return envLevel;
  }

  return "warn";
};

const destination = pino.destination({
  sync: true,
});

export const logger = pino(
  {
    transport:
      process.env.NODE_ENV === "development"
        ? { target: "pino-pretty" }
        : undefined,
    level: getLevel(),
  },
  destination,
);

logger.info(`Start logging with level: "${logger.level}"`);
