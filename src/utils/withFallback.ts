import { mergeAndCompare } from "merge-anything";

export const withFallback = (
  object: Record<string, unknown>,
  fallbacks: Record<string, unknown>,
) =>
  mergeAndCompare(
    (from: unknown, to: unknown) => {
      if (to == null && from != null) {
        return from;
      }
      return to;
    },
    fallbacks,
    object,
  );
