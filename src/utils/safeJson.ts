import type { JsonValue } from "type-fest";

export const safeJsonParse = (jsonLike: unknown): JsonValue | undefined => {
  if (typeof jsonLike !== "string") {
    return undefined;
  }

  try {
    return JSON.parse(jsonLike) as JsonValue;
  } catch {
    return undefined;
  }
};
