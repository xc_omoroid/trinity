/**
 * String.prototype.trim but multiline.
 * @param string String to trim.
 * @returns Trimmed string.
 */
export const trimAll = (string: string): string =>
  string
    .trim()
    // Further reading: https://tc39.es/ecma262/#sec-string.prototype.trim
    .replace(/^\p{Zs}+|\p{Zs}+$/gmu, "");
