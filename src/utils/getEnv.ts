import { logger } from "./logger.js";

export type GetEnvOptions<T> = {
  name: keyof NodeJS.ProcessEnv;
  onFound: (value: string) => T;
  onNotFound: (error: Error) => T;
};

export const getEnv = <T>(options: GetEnvOptions<T>): T => {
  const value = process.env[options.name];

  logger.trace(`$${options.name} is ${value ?? ""}`);

  if (value) {
    return options.onFound(value);
  }

  const error = new Error(
    `$${options.name} does not exist!`,
  ) as NodeJS.ErrnoException;
  error.code = "ENOENT";

  return options.onNotFound(error);
};
