import { toKatakana } from "hepburn";

export const romajiLike =
  /^(?:(?:[bcdfghjklmnpqrstvwxyz]|[kgsztnhbpmr][yh]|bb|cc|dd|ff|gg|hh|jj|kk|ll|mm|nn|pp|qq|rr|ss|tt|vv|ww|xx|yy|zz)?(?:[aeiou]-?|[^bcdfghjklmnpqrstvwxyz](?:n|nn)))+$/i;

export const hasRomaji = (text: string): boolean => {
  const words = new Set(text.match(/[-a-z]+/gi));
  return Array.from(words).some((word) => romajiLike.test(word));
};

export const decodeRomaji = (romaji: string): string =>
  toKatakana(romaji.split("-").join("ー"));

export const decodeRomajiAll = (text: string): string => {
  const words = new Set(text.match(/[-a-z]+/gi));

  const romajiInText = Array.from(words).filter(hasRomaji);

  return romajiInText.reduce(
    (plain, romaji) => plain.split(romaji).join(decodeRomaji(romaji)),
    text,
  );
};
