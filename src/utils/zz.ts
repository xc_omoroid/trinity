import { any } from "zod";

export const resetTo = <T>(value: T) => any().transform(() => value);
