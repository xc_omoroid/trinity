import { Message, EmbedBuilder } from "discord.js";
import type { CommandList } from "../cogs.js";
import { removeVariationSelector, sanitizeCustomEmoji } from "./emoji.js";
import type { Text } from "../tts/tts.js";

const sanitizeMessage = (text: string): string =>
  sanitizeCustomEmoji(removeVariationSelector(text));

type FormatMessageOptions = {
  addAuthor?: boolean;
};

export const getMessageAuthor = (message: Message): string => {
  const member = message.guild?.members.resolve(message.author);

  return (
    member?.displayName ?? message.author.globalName ?? message.author.username
  );
};

export const formatMessage = (
  message: Message,
  options: FormatMessageOptions = {},
): Text => {
  let text = "";

  if (message.cleanContent) {
    text = message.cleanContent;
  } else if (message.attachments) {
    // This code is hopelessly ugly and needs refactor.
    const attachments = {
      images: {
        count: 0,
        spoilerCount: 0,
        name: "画像",
        suffix: "枚",
      },
      videos: {
        count: 0,
        spoilerCount: 0,
        name: "動画",
        suffix: "本",
      },
      others: {
        count: 0,
        spoilerCount: 0,
        name: "その他のファイル",
        suffix: "個",
      },
    };

    message.attachments.forEach((attachment) => {
      if (attachment.contentType) {
        if (attachment.contentType.startsWith("image")) {
          if (attachment.spoiler) {
            attachments.images.spoilerCount++;
          } else {
            attachments.images.count++;
          }
        } else if (attachment.contentType.startsWith("video")) {
          if (attachment.spoiler) {
            attachments.videos.spoilerCount++;
          } else {
            attachments.videos.count++;
          }
        } else {
          if (attachment.spoiler) {
            attachments.others.spoilerCount++;
          } else {
            attachments.others.count++;
          }
        }
      }
    });

    const counts = [];

    for (const { name, count, spoilerCount, suffix } of Object.values(
      attachments,
    )) {
      if (count === 1) {
        counts.push(name);
      } else if (count > 1) {
        counts.push(`${count}${suffix}の${name}`);
      }
      if (spoilerCount === 1) {
        counts.push(`ネタバレ${name}`);
      } else if (spoilerCount > 1) {
        counts.push(`${spoilerCount}${suffix}のネタバレ${name}`);
      }
    }

    if (
      Object.values(attachments).some(
        (attachment) => attachment.count > 0 || attachment.spoilerCount > 0,
      )
    ) {
      text = "添付ファイル、" + counts.join("と、");
    }
  }

  text = text.replace(/\|\|.*?\|\|/g, "、スポイラー、");

  const formattedMessage: Text = [`${sanitizeMessage(text)}。`];

  if (options.addAuthor === true) {
    formattedMessage.unshift(`${getMessageAuthor(message)}。`);
  }

  return formattedMessage;
};

export const formatCommandList = (
  bang: string,
  commandList: CommandList,
): string =>
  commandList
    .filter(
      <T extends CommandList[number]>(command: T): command is Required<T> =>
        !!command.help,
    )
    .map(({ name, help }): string =>
      help ? `\`${bang} ${name}\` - ${help}` : `\`${bang} ${name}\``,
    )
    .join("\n");

export const formatSystemMessage = (message: string) =>
  new EmbedBuilder({ title: "システムメッセージ", description: message });
