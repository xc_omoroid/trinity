import { readFile } from "node:fs/promises";
import type { Dict } from "../dict/index.js";
import { logger } from "./logger.js";
import { createHash } from "node:crypto";
import { trinityDb } from "../db/redis.js";
import {
  type DictEntries,
  dictEntriesToDict,
  getDictEntries,
  setDictEntry,
} from "../dict/persistent.js";

const oldEmojiDbPrefix = "emoji-name-db-";
const emojiHashKey = "emoji-ja-hash-store";
const version = 3;

export const removeVariationSelector = (text: string): string =>
  text.replace(/\ufe0f/g, "");

export const sanitizeCustomEmoji = (text: string): string =>
  text.replace(/<a?:([^:]{2,32}):\d{17,19}>/g, "$1");

const getEmojiDictEntries = (): Promise<DictEntries> =>
  getDictEntries(emojiHashKey, "desc");

const setEmojiDictEntry = (from: string, to: string) =>
  setDictEntry(emojiHashKey, from, to);

export const getEmojiDict = async (): Promise<Dict> =>
  dictEntriesToDict(await getEmojiDictEntries());

export const prepareEmojiStore = async () => {
  const downloadScript = await readFile(
    "./emoji-ja/data/emoji_ja.json",
    "utf8",
  );

  const hash = createHash("sha256");

  hash.update(downloadScript);
  hash.update(`${version}`);

  const digest = hash.digest("hex");

  const dbDigestKey = emojiHashKey + "-digest";

  if ((await trinityDb.get(dbDigestKey)) === digest) {
    logger.info("Emoji store is up to date.");
    return;
  }

  const oldKeys = await trinityDb.keys(oldEmojiDbPrefix + "*");

  if (oldKeys.length > 0) {
    logger.info("Removing outdated emoji data from version <=2...");

    for (const key of oldKeys) {
      logger.trace(`Deleting ${key}...`);
      await trinityDb.del(key);
    }

    logger.info("Removed outdated emoji data.");
  }

  if (await trinityDb.exists(emojiHashKey)) {
    logger.info("Rebuilding emoji store...");
  } else {
    logger.info("Building emoji store...");
  }

  void trinityDb.del(emojiHashKey);

  try {
    const content = await readFile("./emoji-ja/data/emoji_ja.json", "utf8");
    const json = JSON.parse(content) as Record<
      string,
      {
        keywords: string[];
        short_name: string;
        group: string;
        subgroup: string;
      }
    >;
    await Promise.all(
      Object.entries(json).map(async ([emoji, data]) => {
        try {
          if (/\p{Emoji_Presentation}|\p{Extended_Pictographic}/u.test(emoji)) {
            const sanitized = removeVariationSelector(emoji);
            logger.trace(`Setting emoji: ${sanitized}, ${data.short_name}`);
            await setEmojiDictEntry(sanitized, data.short_name);
          } else {
            logger.trace(`Skipping emoji: ${emoji}, ${data.short_name}`);
          }
        } catch (error: unknown) {
          logger.error({ err: error }, "Failed to set emoji kv.");
        }
      }),
    );
  } catch (error: unknown) {
    logger.fatal({ err: error }, "Failed to read from emoji-ja submodule.");
    process.exit(1);
  }

  await trinityDb.set(dbDigestKey, digest);

  logger.info("Built successfully.");
};
