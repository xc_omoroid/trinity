export const second = 1e3;
export const minute = second * 60;
export const hour = minute * 60;
export const day = hour * 24;

export const tinyFormat = (ms: number): string => {
  const dMs = ms - (ms % day);
  const hMs = ms - dMs - (ms % hour);
  const mMs = ms - dMs - hMs - (ms % minute);
  const sMs = ms - dMs - hMs - mMs - (ms % second);

  const result = [
    dMs && `${dMs / day}d`,
    (dMs || hMs) && `${hMs / hour}h`,
    (dMs || hMs || mMs) && `${mMs / minute}m`,
    (dMs || hMs || mMs || sMs) && `${sMs / second}s`,
  ];

  return result.filter(Boolean).join(" ");
};
