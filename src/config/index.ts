import { constants as fsConstants } from "node:fs";
import { access, readFile } from "node:fs/promises";
import { join } from "node:path";
import { cwd } from "node:process";
import { trinityConfig, type TrinityConfigParsed } from "./schema.js";
import { logger } from "../utils/logger.js";
import { parseYamlConfig } from "./yaml.js";

let config: null | TrinityConfigParsed = null;

export const useConfig = async (): Promise<TrinityConfigParsed> => {
  if (config) {
    return config;
  }

  const yamlPath = join(cwd(), "trinity.config.yaml");
  const jsPath = join(cwd(), "trinity.config.js");

  let yamlConfig = null;
  let jsConfig = null;

  try {
    await access(yamlPath, fsConstants.R_OK);
    await access(jsPath, fsConstants.R_OK);

    logger.warn(
      "Both yaml and js config files are found. Trinity will ignore the latter one.",
    );
    // eslint-disable-next-line no-empty
  } catch {}

  try {
    yamlConfig = await readFile(yamlPath, "utf8");
  } catch {
    jsConfig = await import(jsPath);
  }

  let unparsedConfig;

  if (yamlConfig !== null) {
    unparsedConfig = parseYamlConfig(yamlConfig);
  } else {
    if ("default" in jsConfig) {
      unparsedConfig = jsConfig.default;
    } else {
      unparsedConfig = jsConfig;
    }
  }

  config = trinityConfig.parse(unparsedConfig);

  logger.trace({ config }, "Using config");

  return config;
};
