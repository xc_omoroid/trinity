import { DEFAULT_SCHEMA, loadAll, Type } from "js-yaml";
import { z } from "zod";
import { type TTSPresets, ttsPresets } from "./schema.js";
import { Presets } from "../utils.js";

const yamlPresets = z
  .object({
    default: z.string().optional(),
    choices: z.string().or(z.string().array()).optional(),
    from: z.string(),
  })
  .strict();

const presetsDocument = z.record(z.string(), ttsPresets);

const presetsStore = new Map<string, TTSPresets>();

const PresetsSetYamlType = new Type("!pset", {
  kind: "mapping",
  construct: (data: unknown) => {
    const raw = presetsDocument.parse(data);

    Object.entries(raw).forEach(([key, presets]) => {
      presetsStore.set(key, presets);
    });
  },
});

const PresetsGetYamlType = new Type("!pget", {
  kind: "mapping",
  construct: (data: unknown) => {
    const raw = yamlPresets.parse(data);

    const reference = presetsStore.get(raw.from);

    if (reference == null) {
      throw new Error(`Failed to resolve presets ${raw.from}.`);
    }

    let presets = new Presets(reference);

    if (raw.choices) {
      if (Array.isArray(raw.choices)) {
        presets = presets.pick(...raw.choices);
      } else {
        presets = presets.pick(raw.choices);
      }
    }

    if (raw.default) {
      presets = presets.setDefault(raw.default);
    }

    return presets;
  },
  instanceOf: Presets,
});

const configSchema = DEFAULT_SCHEMA.extend([
  PresetsSetYamlType,
  PresetsGetYamlType,
]);

export const parseYamlConfig = (yaml: string) => {
  let config: unknown = undefined;

  let documentLength = 0;

  loadAll(
    yaml,
    (doc) => {
      documentLength++;

      if (documentLength > 2) {
        new Error("Expected 1 or 2 YAML documents.");
      }

      config = doc;
    },
    { schema: configSchema },
  );

  return config;
};
