import { z } from "zod";

const maxMessageLength = z.number().int().min(0).max(50000);

const openJtalkConfig = z.object({
  engine: z.literal("open_jtalk"),
  htsvoicePath: z.string().min(1),
});

const voicevoxConfig = z.object({
  engine: z.literal("voicevox"),
  speakerName: z.string().min(1),
  speakerStyle: z.string().min(1),
  speedDelta: z.number().optional(),
  pitchDelta: z.number().optional(),
  intoneDelta: z.number().optional(),
  volumeDelta: z.number().optional(),
});

const voicePresetLabelMeta = z.object({
  label: z.string().min(1),
  default: z.boolean().optional(),
});

export const singleTTSConfig = z.discriminatedUnion("engine", [
  openJtalkConfig,
  voicevoxConfig,
]);

export const singleTTSPreset = z.intersection(
  singleTTSConfig,
  voicePresetLabelMeta,
);

export const ttsPresets = z.preprocess((unknown) => {
  if (Array.isArray(unknown)) {
    return Array.from(unknown) as unknown[];
  }

  return unknown;
}, z.array(singleTTSPreset).nonempty());

const ttsConfig = z.union([singleTTSConfig, ttsPresets]);

export type OpenJTalkConfig = z.infer<typeof openJtalkConfig>;
export type VoicevoxConfig = z.infer<typeof voicevoxConfig>;
export type SingleTTSConfig = z.infer<typeof singleTTSConfig>;
export type SingleTTSPreset = z.infer<typeof singleTTSPreset>;
export type TTSPresets = z.infer<typeof ttsPresets>;
export type TTSConfig = z.infer<typeof ttsConfig>;

const clientConfig = z.object({
  command: z.string().min(1),
  token: z.string().min(1),
  tts: ttsConfig.optional(),
  maxMessageLength: maxMessageLength.optional(),
});

export type ClientConfig = z.infer<typeof clientConfig>;

export const trinityConfig = z
  .object({
    tts: ttsConfig.optional(),
    maxMessageLength: maxMessageLength.default(60),
    clients: z.array(clientConfig).nonempty(),
  })
  .refine(
    (root) => {
      if (!root.tts && root.clients.some((client) => !client.tts)) {
        return false;
      }
      return true;
    },
    {
      message: "Unable to fallback to global tts configuration.",
      path: ["tts"],
    },
  )
  .superRefine((root, ctx) => {
    if (Array.isArray(root.tts)) {
      const labels = new Set<string>();

      root.tts.forEach((value, index) => {
        if (labels.has(value.label)) {
          ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: `Duplicated label ${value.label} detected.`,
            path: ["tts", index],
            fatal: true,
          });
        }

        labels.add(value.label);
        return true;
      });
    }

    root.clients.forEach((client, clientIndex) => {
      if (Array.isArray(client.tts)) {
        const clientLabels = new Set<string>();

        client.tts.forEach((value, ttsIndex) => {
          if (clientLabels.has(value.label)) {
            ctx.addIssue({
              code: z.ZodIssueCode.custom,
              message: `Duplicated label ${value.label} detected.`,
              path: ["clients", clientIndex, "tts", ttsIndex],
            });
          }
        });
      }
    });
  })
  .transform((root) => ({
    ...root,
    clients: root.clients.map((client) => {
      const transformed = { ...client };
      let tts: TTSConfig;

      if (!client.tts) {
        tts = root.tts!;
      } else {
        tts = client.tts;
      }
      if (!client.maxMessageLength) {
        transformed.maxMessageLength = root.maxMessageLength!;
      }

      return { ...transformed, tts };
    }),
  }));

export type TrinityConfig = z.input<typeof trinityConfig>;
export type TrinityConfigParsed = z.output<typeof trinityConfig>;
