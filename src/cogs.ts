import type { Message } from "discord.js";
import { logger } from "./utils/logger.js";
import { trimAll } from "./utils/trimAll.js";

export type Command = {
  help?: string;
  replacement?: string;
  callback: (message: Message, rest?: string) => void;
};

export type CommandList = {
  name: string;
  help?: string;
}[];

export const Cogs = class {
  bang: string;

  commands: Map<string, Command>;

  register(name: string, command: Command): boolean {
    if (this.commands.has(name)) {
      return false;
    }

    if (name.includes(" ")) {
      throw new Error("Registration of subcommands is not supported.");
    }

    this.commands.set(name, command);
    return true;
  }

  unregister(name: string): boolean {
    this.commands.delete(name);
    return false;
  }

  help(name: string): string | undefined {
    return this.commands.get(name)?.help;
  }

  list(): CommandList {
    const commands = [...this.commands.entries()];

    return commands.map(([name, { help }]) =>
      help ? { name, help } : { name },
    );
  }

  async handler(message: Message): Promise<void> {
    if (!message.content.startsWith(this.bang)) {
      return;
    }

    const contentWithoutBang = message.content
      .slice(this.bang.length)
      .trimStart();

    const [head, body] = contentWithoutBang.split(/\s+(.*)/);

    if (head) {
      logger.trace(
        {
          head,
          body,
        },
        "Parsed cog command.",
      );

      let command = this.commands.get(head);

      if (!command) {
        const wildcard = this.commands.get("*");
        if (wildcard) {
          command = wildcard;
        } else {
          return;
        }
      }

      try {
        await message.reply(
          trimAll(`
              ドットコマンド (\`${
                this.bang
              } ${head}\`) は非推奨で、今後削除されます。
              ${command.replacement ? `代替: \`${command.replacement}\`` : ``}
            `),
        );

        if (command.callback.length <= 1 && !body) {
          command.callback(message);
        } else if (command.callback.length >= 2) {
          command.callback(message, body);
        }
      } catch (error: unknown) {
        logger.error({ err: error }, "Failed to run command.");
      }
    }
  }

  constructor(bang: string) {
    if (bang.includes(" ")) {
      throw new Error(`Invalid character \\u0020 at ${bang.indexOf(" ")}`);
    }

    this.bang = bang;
    this.commands = new Map();
  }
};
