import * as Discord from "discord.js";
import {
  generateDependencyReport,
  getVoiceConnection,
  VoiceConnectionStatus,
} from "@discordjs/voice";
import {
  formatCommandList,
  formatSystemMessage,
} from "./utils/formatMessage.js";
import { prepareEmojiStore } from "./utils/emoji.js";
import { logger } from "./utils/logger.js";
import { Cogs } from "./cogs.js";
import { trimAll } from "./utils/trimAll.js";
import { tinyFormat } from "./utils/tinyDate.js";
import { useConfig } from "./config/index.js";
import { createHash } from "node:crypto";
import { closeDb, openDb } from "./db/redis.js";
import {
  getCustomDictEntries,
  removeCustomDictEntry,
  setCustomDictEntry,
} from "./dict/custom.js";
import { createTTSManager, type TTSManager } from "./tts/manager.js";
import { executeInteraction } from "./interactions/index.js";
import {
  ApplicationFlags as ApplicationFlagBits,
  OAuth2Scopes,
  PermissionFlagsBits,
  Routes,
  GatewayIntentBits,
} from "discord-api-types/v10";
import { REST } from "@discordjs/rest";
import {
  hashFromTTSConfig,
  setTTSConfigCache,
} from "./db/ttsConfigHashCache.js";
import type { ClientConfig, TTSConfig } from "./config/schema.js";
import { getVersion } from "./utils.js";
import { syncApplicationCommands } from "./interactions/deploy.js";
import { getGuildOverwrites } from "./db/localOverwrites.js";
import { ChannelType } from "discord.js";
import columnify from "columnify";

let deferRan = false;

const defer = async () => {
  if (deferRan) {
    return;
  }

  deferRan = true;

  logger.info("Shutdown gracefully...");
  await closeDb();
};

process.on("SIGINT", defer);
process.on("SIGHUP", defer);
process.on("SIGTERM", defer);

const globalErrorHandler = (kind: string) => (error: unknown) => {
  logger.error({ err: error }, `Caught unhandled ${kind}`);
};

if (process.env.NODE_ENV === "production") {
  process.on("uncaughtException", globalErrorHandler("exception"));
  process.on("unhandledRejection", globalErrorHandler("rejection"));
}

export const Trinity = class {
  static readonly set = new Set();

  readonly client: Discord.Client;

  readonly bang: string;

  private readonly token: string;

  readonly tokenHash: string;

  readonly ttsConfig: TTSConfig;

  readonly ttsManager: TTSManager;

  readonly permissions: Discord.PermissionsBitField;

  readonly scopes: Discord.OAuth2Scopes[];

  readonly ttsConfigHash: string;

  login() {
    void this.client.login(this.token);
  }

  constructor(config: ClientConfig) {
    const token = config.token.trim();

    this.bang = config.command.trim();
    this.token = token;
    this.tokenHash = createHash("sha256").update(token).digest("hex");

    if (!config.tts) {
      throw new Error(`No tts config found on ${this.bang}`);
    }

    this.ttsConfig = config.tts;
    this.ttsConfigHash = hashFromTTSConfig(this.ttsConfig);

    setTTSConfigCache(this.ttsConfigHash, this.tokenHash);

    this.client = new Discord.Client({
      intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildVoiceStates,
        GatewayIntentBits.MessageContent,
      ],
    });

    this.permissions = new Discord.PermissionsBitField([
      PermissionFlagsBits.ViewChannel,
      PermissionFlagsBits.SendMessages,
      PermissionFlagsBits.SendMessagesInThreads,
      PermissionFlagsBits.EmbedLinks,
      PermissionFlagsBits.AttachFiles,
      PermissionFlagsBits.ReadMessageHistory,
      PermissionFlagsBits.MentionEveryone,
      PermissionFlagsBits.Connect,
      PermissionFlagsBits.Speak,
      PermissionFlagsBits.UseVAD,
      PermissionFlagsBits.PrioritySpeaker,
    ]);

    this.scopes = [OAuth2Scopes.Bot, OAuth2Scopes.ApplicationsCommands];

    this.ttsManager = createTTSManager(this);

    const cogs = new Cogs(this.bang);

    cogs.register("join", {
      help: "現在のボイスチャットに接続し、現在のテキストチャンネルに投稿された文章の読み上げを開始します。",
      replacement: "/vc join",
      callback: async (message) => {
        if (!message.member) {
          logger.error(
            { content: message.content, id: message.id },
            "No member available.",
          );
          return;
        }

        if (!message.guild) {
          void message.reply("このコマンドはサーバー内でのみ使用できます。");
          return;
        }

        if (message.channel.type === ChannelType.DM) {
          void message.reply("このコマンドはDMでは使用できません");
          return;
        }

        if (message.channel.type === ChannelType.GuildStageVoice) {
          void message.reply(
            "このコマンドはステージチャンネルでは使用できません",
          );
          return;
        }

        const { channel: voiceChannel } = message.member.voice;

        if (!voiceChannel) {
          void message.reply(
            "このコマンドを実行するには、いずれかのボイスチャンネルに所属する必要があります。",
          );
          return;
        }

        const voiceConnectionStatus = getVoiceConnection(
          message.guild.id,
          this.tokenHash,
        )?.state.status;

        if (
          voiceConnectionStatus === VoiceConnectionStatus.Disconnected ||
          voiceConnectionStatus === VoiceConnectionStatus.Destroyed
        ) {
          logger.info("Cleaning up voice connection.");
          await this.ttsManager.remove(message.guild.id);
        }

        const currentTTSData = await this.ttsManager.get(message.guild.id);

        if (currentTTSData) {
          if (
            message.channelId === currentTTSData.ttsState.textChannelId &&
            message.member.voice.channelId ===
              currentTTSData.ttsState.voiceChannelId
          ) {
            void message.reply(
              `${
                this.client.user?.username || "このBOT"
              }は既にこのチャンネルに登録されています。`,
            );
            return;
          }

          let configuredChannelName = "別のチャンネル";

          if (message.channelId !== currentTTSData.ttsState.textChannelId) {
            const configuredTextChannel = await message.guild.channels.fetch(
              currentTTSData.ttsState.textChannelId,
            );

            if (
              configuredTextChannel != null &&
              configuredTextChannel.type === ChannelType.GuildText
            ) {
              configuredChannelName = `テキストチャンネル \`${Discord.escapeInlineCode(
                configuredTextChannel.name,
              )}\` `;
            } else if (
              configuredTextChannel != null &&
              configuredTextChannel.type === ChannelType.GuildVoice
            ) {
              configuredChannelName = `ボイスチャンネル \`${Discord.escapeInlineCode(
                configuredTextChannel.name,
              )}\` 内のテキストチャット`;
            } else {
              configuredChannelName = "別のテキストチャンネル";
            }
          } else if (
            message.member.voice.channelId !==
            currentTTSData.ttsState.voiceChannelId
          ) {
            const configuredVoiceChannel = await message.guild.channels.fetch(
              currentTTSData.ttsState.voiceChannelId,
            );

            if (
              configuredVoiceChannel != null &&
              configuredVoiceChannel.type === ChannelType.GuildVoice
            ) {
              configuredChannelName = `ボイスチャンネル \`${Discord.escapeInlineCode(
                configuredVoiceChannel.name,
              )}\` `;
            } else {
              configuredChannelName = "別のボイスチャンネル";
            }
          }

          void message.reply(
            trimAll(`
              ${
                this.client.user?.username ?? "このBOT"
              }は${configuredChannelName}に登録されています。
              このチャンネルで使いたい場合は \`/vc bye\` で切断してから再接続してください。
            `),
          );
          return;
        }

        const ttsData = await this.ttsManager.connect(
          message.id,
          message.channel.id,
          voiceChannel.id,
        );

        if (!ttsData) {
          logger.error(
            {
              messageChannelId: message.channel.id,
              voiceChannelId: voiceChannel.id,
            },
            "Failed to initialize TTS.",
          );
          return;
        }

        const systemMessage = await message.channel.send({
          embeds: [formatSystemMessage("ボイスチャットに接続しました。")],
        });

        await this.ttsManager.updateState(message.guild.id, {
          lastAddedMessageId: systemMessage.id,
          lastAddedUserId: systemMessage.author.id,
          lastPlayedMessageId: systemMessage.id,
          lastPlayedUserId: systemMessage.author.id,
        });

        logger.info(
          {
            channel: voiceChannel.name,
            lengths: await this.ttsManager.getLengths(),
          },
          "Registered TTS instance.",
        );
      },
    });

    cogs.register("bye", {
      help: "読み上げを停止し、ボイスチャットから切断します。",
      replacement: "/vc bye",
      callback: async (message) => {
        if (!message.guild) {
          void message.reply("ボイスチャットに接続していません。");
          return;
        }

        if (message.channel.type === ChannelType.GuildStageVoice) {
          void message.reply(
            "このコマンドはステージチャンネルでは使用できません",
          );
          return;
        }

        const ttsManagerData = await this.ttsManager.get(message.guild.id);

        if (
          ttsManagerData &&
          ttsManagerData.ttsState.textChannelId !== message.channel.id
        ) {
          logger.debug(
            {
              channelA: ttsManagerData.ttsState.textChannelId,
              channelB: message.channel.id,
            },
            "Called bye from unrelated voice channel",
          );

          void message.reply(
            `${
              this.client.user?.username || "このBOT"
            }はこのチャンネルに未接続です。`,
          );

          return;
        }

        if (getVoiceConnection(message.guild.id, this.tokenHash)) {
          await this.ttsManager.disconnect(message.guild.id);

          void message.channel.send({
            embeds: [formatSystemMessage("ボイスチャットから切断しました。")],
          });
        } else {
          logger.debug({ guild: message.guild.name }, "Already disconnected.");
          void message.reply("ボイスチャットに接続していません。");
        }
      },
    });

    cogs.register("teach", {
      help: `単語の読みを教えます。\`${this.bang} teach 四月一日 わたぬき\` のように使います。`,
      replacement: "/dict teach",
      callback: async (message, rest) => {
        if (!message.guild) {
          void message.reply("サーバー外から単語を登録することはできません。");
          return;
        }

        const regexp =
          /^(?<from>\S+)\s+(?<to>(?:\p{scx=Hira}|\p{scx=Kana})+)$/u;

        if (!rest) {
          logger.error(
            { content: message.cleanContent },
            "No kanji-read record.",
          );

          void message.reply(
            trimAll(`
              単語と読みが与えられていません。
              \`${this.bang} teach 小鳥遊 たかなし\` のように登録するキーワードを指定してください。
            `),
          );

          return;
        }

        const body = rest.trim().toLowerCase();
        const result = regexp.exec(body);

        if (!result?.groups || !result.groups["from"] || !result.groups["to"]) {
          logger.error({ body }, "Invalid kanji-read record.");

          void message.reply(
            trimAll(`
              単語と読みの形式が不正です。
              \`${this.bang} teach 不知火 しらぬい\` のように登録するキーワードを指定してください。
            `),
          );

          return;
        }

        const { from, to } = result.groups;

        logger.trace("Adding kanji-read record.");

        await setCustomDictEntry(message.guild.id, from, to);

        void message.reply(
          `\`${Discord.escapeInlineCode(
            from ?? "",
          )}\` は \`${Discord.escapeInlineCode(to ?? "")}\` を登録しました。`,
        );
      },
    });

    cogs.register("forget", {
      help: `覚えさせた単語の読みを忘れさせます。\`${this.bang} forget 八月一日\` のように使います。`,
      replacement: "/dict forget",
      callback: async (message, rest) => {
        if (!message.guild) {
          void message.reply("辞書機能はサーバー外からは利用できません。");
          return;
        }

        if (!rest) {
          void message.reply(
            trimAll(`
              単語が指定されていません。
              \`${this.bang} forget 小此木\` のように使います。
            `),
          );

          return;
        }

        const body = rest.trim().toLowerCase();

        const dictEntries = await getCustomDictEntries(message.guild.id);
        const found = dictEntries.find((dict) => dict[0] === body);

        if (!found) {
          const scores = dictEntries
            .map((entry): [number, [string, string]] => {
              let score = -Infinity;
              if (entry[0].startsWith(body)) {
                score = -entry[0].length;
              } else if (body.startsWith(entry[0])) {
                score = -body.length;
              }
              return [score, entry];
            })
            .sort((aEntry, bEntry) => bEntry[0] - aEntry[0])
            .filter((entry) => Number.isFinite(entry[0]));

          logger.trace({ scores }, "Scored dictionary entry distances.");

          if (scores[0] != null) {
            void message.reply(
              trimAll(`
                \`${Discord.escapeInlineCode(body)}\` は登録されていません。
                もしかして: \`${Discord.escapeInlineCode(scores[0][1][0])}\`
              `),
            );
            return;
          }

          void message.reply(
            `\`${Discord.escapeInlineCode(body)}\` は登録されていません。`,
          );
          return;
        }

        const [from, to] = found;

        await removeCustomDictEntry(message.guild.id, from);

        void message.reply(
          `\`${Discord.escapeInlineCode(
            from,
          )}\` は \`${Discord.escapeInlineCode(to)}\` を忘れました。`,
        );
      },
    });

    cogs.register("ping", {
      help: '"pong!" と返信します。主にBOTが正しく動作しているかのテストに使われます。',
      callback: (message) => {
        logger.trace("Returning pong.");
        void message.reply("pong!");
      },
    });

    cogs.register("list", {
      help: "使用可能なコマンドのリストを出力します。",
      callback: (message) => {
        void message.reply(formatCommandList(this.bang, cogs.list()));
      },
    });

    cogs.register("help", {
      help: `コマンドのヘルプを表示します。\`${this.bang} help list\` のように使います。`,
      callback: (message, args) => {
        if (!args) {
          void message.reply(
            trimAll(`
              以下のコマンドが使用可能です。
              \`/vc join\` - 現在のボイスチャットに接続し、現在のテキストチャンネルに投稿された文章の読み上げを開始します。
              \`/vc bye\` - 読み上げを停止し、ボイスチャットから切断します。
              \`/dict teach\` - 単語の読みを教えます。\`/dict teach 四月一日 わたぬき\` のように使います。
              \`/dict forget\` - 覚えさせた単語の読みを忘れさせます。\`/dict forget 八月一日\` のように使います。
              \`/config\` - BOTの設定画面を開きます。声の設定などを変更できます。
              \`/info\` - BOTの状態やソースコードなどの情報を表示します。
            `),
          );
          return;
        }

        const commandName = args.trim();
        const command = cogs.commands.get(commandName);

        if (!command || !command.help) {
          void message.reply(
            `コマンド \`${Discord.escapeInlineCode(
              commandName,
            )}\` は存在しません。`,
          );
          return;
        }

        void message.reply(
          formatCommandList(this.bang, [
            { name: commandName, help: command.help },
          ]),
        );
      },
    });

    cogs.register("status", {
      help: "BOTのステータスを表示します。",
      replacement: "/info",
      callback: async (message) => {
        const currentGuild = message.guildId;

        const allClients = [...Trinity.set.values()].filter(
          (unknown): unknown is InstanceType<typeof Trinity> =>
            unknown instanceof Trinity,
        );

        let pickedClients: InstanceType<typeof Trinity>[];

        if (currentGuild === null) {
          pickedClients = [this];
        } else {
          pickedClients = allClients.filter(
            (trinity) => trinity.client.guilds.resolve(currentGuild) !== null,
          );
        }

        const status = await Promise.all(
          pickedClients.map(async (trinity) => ({
            INSTANCE: trinity.client.user?.username ?? trinity.bang,
            STATUS: Discord.Status[trinity.client.ws.status] ?? "Unknown",
            UPTIME: tinyFormat(trinity.client.uptime || 0),
            VCSIZE: `${(await trinity.ttsManager.getLengths()).instanceLength}`,
          })),
        );

        void message.reply(
          trimAll(`
            Status of \`Trinity@${Discord.escapeInlineCode(
              await getVersion(),
            )}\`
            \`\`\`plain
            ${columnify(status)}
            \`\`\`
          `),
        );
      },
    });

    cogs.register("licenses", {
      help: "ライセンス情報を投稿します。",
      replacement: "/info",
      callback: (message) => {
        logger.trace("Printing license...");
        void message.reply({
          files: [
            "./LICENSE.txt",
            "./OPEN_JTALK.txt",
            "./VOICEVOX.txt",
            "./VOICEVOX_ENGINE.txt",
            "./VENDORS.txt",
          ],
        });
      },
    });

    this.client.on("messageCreate", async (message) => {
      if (!this.client.user) {
        return;
      }

      if (
        new RegExp(`<@!?${this.client.user.id}>\\s+health\\s*check`).test(
          message.content,
        )
      ) {
        const checkIntent = (): string => {
          if (!this.client.application) {
            return "unknown";
          }

          const qualified = this.client.application.flags.any([
            ApplicationFlagBits.GatewayMessageContentLimited,
            ApplicationFlagBits.GatewayMessageContentLimited,
          ]);

          if (!qualified) {
            return `requires GATEWAY_MESSAGE_CONTENT or GATEWAY_MESSAGE_CONTENT_LIMITED`;
          }

          return "ok";
        };

        const checkScope = async (): Promise<string> => {
          const rest = new REST({ version: "10" }).setToken(token);

          try {
            if (!this.client.application || !message.guild) {
              return "unknown";
            }

            await rest.get(
              Routes.applicationGuildCommands(
                this.client.application.id,
                message.guild.id,
              ),
            );

            return "ok";
          } catch (error: unknown) {
            return "missing applications.commands";
          }
        };

        const checkGuildPermissions = (): string => {
          if (!message.guild?.members.me) {
            return "unknown";
          }

          const missing = message.guild.members.me.permissions.missing(
            this.permissions,
          );

          logger.debug({ missing }, "Detected missing permissions.");

          if (missing && missing.length > 0) {
            return `missing ${missing.join()}`;
          }

          return "ok";
        };

        const checkChannelPermissions = (): string => {
          if (
            !message.inGuild() ||
            (message.channel.type !== ChannelType.GuildText &&
              message.channel.type !== ChannelType.GuildVoice) ||
            message.guild.members.me == null
          ) {
            return "unknown";
          }

          const missing = message.guild.members.me
            .permissionsIn(message.channel)
            .missing(this.permissions);

          if (missing && missing.length > 0) {
            return "missing " + missing.join();
          }

          return "ok";
        };

        void message.reply(
          trimAll(`
            application intents... ${checkIntent()}
            invite scope... ${await checkScope()}
            guild permissions... ${checkGuildPermissions()}
            channel permissions... ${checkChannelPermissions()}
          `),
        );
      }
    });

    this.client.on("messageCreate", (message) => {
      try {
        void this.ttsManager.addMessage(message);
      } catch (err: unknown) {
        logger.error({ err }, "Error caught on ttsManager.");
      }

      try {
        void cogs.handler(message);
      } catch (err: unknown) {
        logger.error({ err }, "Error caught on cogs.");
      }
    });

    this.client.on("interactionCreate", (interaction) => {
      void executeInteraction({ client: this, interaction });
    });

    this.client.on("voiceStateUpdate", async (previous, next) => {
      const ttsData = await this.ttsManager.get(previous.guild.id);

      if (!ttsData) {
        return;
      }

      if (
        next.member != null &&
        next.member.id === this.client.user?.id &&
        next.channelId != null &&
        next.channelId !== previous.channelId &&
        next.channelId !== ttsData.ttsState.voiceChannelId
      ) {
        logger.debug(
          {
            previous: previous.channelId,
            next: next.channelId,
          },
          "Changed voice channel unexpectedly.",
        );

        void this.ttsManager.updateState(next.guild.id, {
          voiceChannelId: next.channelId,
        });

        const textChannel = await previous.guild.channels.fetch(
          ttsData.ttsState.textChannelId,
        );

        if (
          textChannel &&
          (textChannel.type === ChannelType.GuildText ||
            textChannel.type === ChannelType.GuildVoice)
        ) {
          void this.ttsManager.addPlainText(
            next.guild.id,
            "ボイスチャンネルを、変更しました",
          );

          void textChannel.send({
            embeds: [
              formatSystemMessage(
                trimAll(`
                  ユーザーの操作によりボイスチャンネルを変更しました。
                  \`${Discord.escapeInlineCode(
                    previous.channel?.name ?? "取得失敗",
                  )}\` → \`${Discord.escapeInlineCode(
                    next.channel?.name ?? "取得失敗",
                  )}\``),
              ),
            ],
          });
        } else {
          logger.warn(
            { textChannelId: ttsData.ttsState.textChannelId },
            "Text channel not found.",
          );
        }
      }

      if (
        previous.channelId == null &&
        next.channelId != null &&
        next.channelId === ttsData.ttsState.voiceChannelId &&
        next.member != null &&
        next.member.id !== this.client.user?.id
      ) {
        logger.debug(
          {
            member: next.member?.displayName,
          },
          "Member joined to the voice channel.",
        );

        const guildOverwrites = await getGuildOverwrites({
          tokenHash: this.tokenHash,
          guildId: previous.guild.id,
        });

        if (guildOverwrites.notifyUserActivities) {
          const name = previous.member?.displayName;

          void this.ttsManager.addPlainText(
            previous.guild.id,
            `${name ? name + "さん" : "一名"}が入室しました。`,
          );
        }
      }

      if (
        previous.channelId != null &&
        next.channelId == null &&
        previous.channelId === ttsData.ttsState.voiceChannelId &&
        previous.member != null &&
        previous.member.id !== this.client.user?.id
      ) {
        logger.debug(
          {
            member: next.member?.displayName,
          },
          "Member left from the voice channel.",
        );

        const guildOverwrites = await getGuildOverwrites({
          tokenHash: this.tokenHash,
          guildId: previous.guild.id,
        });

        if (guildOverwrites.notifyUserActivities) {
          const name = previous.member?.displayName;

          void this.ttsManager.addPlainText(
            previous.guild.id,
            `${name ? name + "さん" : "一名"}が退室しました。`,
          );
        }
      }

      if (
        previous.channelId != null &&
        previous.channelId === ttsData.ttsState.voiceChannelId &&
        previous.channel?.members.every((member) => member.user.bot)
      ) {
        logger.info(
          {
            previousMembersSize: previous.channel?.members.size,
            previousMembers: previous.channel?.members.map(
              (member) => member.displayName,
            ),
            nextMembersSize: next.channel?.members.size,
            nextMembers: next.channel?.members.map(
              (member) => member.displayName,
            ),
          },
          "Disconnecting due to inactivity.",
        );

        await this.ttsManager.disconnect(previous.guild.id);

        const textChannel = await previous.guild.channels.fetch(
          ttsData.ttsState.textChannelId,
        );

        if (
          !textChannel ||
          (textChannel.type !== ChannelType.GuildText &&
            textChannel.type !== ChannelType.GuildVoice)
        ) {
          logger.warn(
            { textChannelId: ttsData.ttsState.textChannelId },
            "Text channel not found.",
          );
          return;
        }

        void textChannel.send({
          embeds: [
            formatSystemMessage(
              "ユーザーがいないため、ボイスチャットから切断しました。",
            ),
          ],
        });
      } else if (
        previous.channelId != null &&
        previous.member != null &&
        next.channelId == null &&
        previous.member.id === this.client.user?.id
      ) {
        logger.info("Connection lost.");

        await this.ttsManager.disconnect(previous.guild.id);

        const textChannel = await previous.guild.channels.fetch(
          ttsData.ttsState.textChannelId,
        );

        if (
          !textChannel ||
          (textChannel.type !== ChannelType.GuildText &&
            textChannel.type !== ChannelType.GuildVoice)
        ) {
          logger.warn(
            { textChannelId: ttsData.ttsState.textChannelId },
            "Text channel not found.",
          );
          return;
        }

        void textChannel.send({
          embeds: [formatSystemMessage("ボイスチャットから切断されました。")],
        });
      }
    });

    this.client.on("guildCreate", (guild) => {
      logger.info(
        { id: guild.id, owner: guild.ownerId, name: guild.name },
        "Bot joined a guild",
      );
    });

    this.client.on("ready", () => {
      if (!this.client.user) {
        // type guard
        return;
      }

      logger.info(`${this.client.user.tag} successfully set up!`);

      if (process.env.NODE_ENV === "development") {
        logger.debug(
          this.client.user.setStatus("invisible"),
          "Running in development mode...",
        );
      }

      this.client.user.setPresence({
        activities: [{ name: `${this.bang} help` }],
      });

      const invite = this.client.generateInvite({
        scopes: this.scopes,
        permissions: this.permissions,
      });

      logger.info(
        `An invite link ${invite} generated for ${this.client.user.tag}.`,
      );

      void this.ttsManager.resume();
    });

    this.client.on("error", (error) =>
      logger.error({ err: error }, "Discord.js encountered an error."),
    );

    Trinity.set.add(this);

    const logout = async () => {
      logger.info(`Logging out ${this.client.user?.tag ?? this.bang}`);
      await this.client.destroy();
    };

    process.on("SIGINT", logout);
    process.on("SIGHUP", logout);
    process.on("SIGTERM", logout);
  }
};

void useConfig().then(async (config) => {
  await openDb();
  await prepareEmojiStore();

  logger.debug(generateDependencyReport());

  void syncApplicationCommands(config);

  config.clients.forEach((clientConfig) => {
    try {
      const bot = new Trinity(clientConfig);
      bot.login();
    } catch (error: unknown) {
      logger.error({ err: error }, "Failed to start discord bot!");
    }
  });
});
