import { readFile } from "node:fs/promises";
import { join } from "node:path";
import { cwd } from "node:process";
import { z } from "zod";
import type { SingleTTSPreset, trinityConfig } from "./config/schema.js";
import { safeJsonParse } from "./utils/safeJson.js";

export const config = <T extends z.input<typeof trinityConfig>>(
  options: T,
): T => options;

export const Presets = class<T extends SingleTTSPreset> extends Array<T> {
  constructor(presets: readonly T[]) {
    super();

    if (presets.length === 0) {
      throw new Error("Called constructor with no presets.");
    }

    this.push(...presets);
  }

  copy() {
    if (this.length === 0) {
      throw new Error("No presets set.");
    }

    return new Presets([...this]);
  }

  pick(...labels: T["label"][]) {
    const presets: T[] = [];

    for (const label of labels) {
      const found = this.find((preset) => preset.label === label);
      if (found) {
        presets.push(found);
      } else {
        throw new Error(`The given label ${label} is invalid.`);
      }
    }

    if (presets.length === 0) {
      throw new Error("No valid preset labels given.");
    }

    return new Presets(presets);
  }

  setDefault(label: T["label"]) {
    if (this.length === 0) {
      throw new Error("Unable to set default with no presets.");
    }

    let set = false;

    const presets: T[] = [];

    this.forEach((preset) => {
      const copy = { ...preset };
      if (copy.label === label) {
        copy.default = true;
        set = true;
      } else {
        copy.default = false;
      }
      presets.push(copy);
    });

    if (!set) {
      throw new Error(`Preset label ${label} not found.`);
    }

    return new Presets(presets);
  }
};

let version: string;
export const getVersion = async (): Promise<string> => {
  if (version) {
    return version;
  }

  const packageJson = z
    .preprocess(
      safeJsonParse,
      z.object({
        version: z.string(),
      }),
    )
    .safeParse(await readFile(join(cwd(), "package.json"), "utf8"));

  if (packageJson.success) {
    version = packageJson.data.version;
  } else {
    version = "0.0.0";
  }

  return version;
};
