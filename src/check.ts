import { ZodError } from "zod";
import { useConfig } from "./config/index.js";
import { logger } from "./utils/logger.js";

if (!logger.isLevelEnabled("info")) {
  logger.level = "info";
}

useConfig()
  .then(() => {
    logger.info("Config file seems fine.");
  })
  .catch((error: unknown) => {
    process.exitCode = 1;
    if (error instanceof ZodError) {
      logger.error({ issues: error.issues }, "Config file seems problematic.");
    } else {
      logger.error(error);
    }
  });
