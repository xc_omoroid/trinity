FROM node:20.2.0-bullseye

ENV CI=true

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    build-essential \
    ffmpeg \
    gosu && \
    chmod +s /usr/sbin/gosu && \
    rm -rf /var/lib/apt/lists/*

RUN corepack enable && \
    corepack prepare pnpm@8.6.1 --activate


# Install HTS Engine API

WORKDIR /tmp

RUN wget --progress=dot:giga https://downloads.sourceforge.net/hts-engine/hts_engine_API-1.10.tar.gz && \
    tar xf hts_engine_API-1.10.tar.gz

WORKDIR /tmp/hts_engine_API-1.10

RUN ./configure && \
    make && \
    make install && \
    rm -rf /tmp/hts_engine_API-1.10.tar.gz /tmp/hts_engine_API-1.10

# Install Open JTalk binary

WORKDIR /tmp

RUN wget --progress=dot:giga https://downloads.sourceforge.net/open-jtalk/open_jtalk-1.11.tar.gz && \
    tar xf open_jtalk-1.11.tar.gz

WORKDIR /tmp/open_jtalk-1.11

RUN ./configure && \
    make && \
    make install && \
    rm -rf /tmp/open_jtalk-1.11.tar.gz /tmp/open_jtalk-1.11

# Install default voice (mei_normal)

WORKDIR /tmp

RUN wget --progress=dot:giga https://downloads.sourceforge.net/mmdagent/MMDAgent_Example/MMDAgent_Example-1.8/MMDAgent_Example-1.8.zip && \
    unzip MMDAgent_Example-1.8.zip

WORKDIR /tmp/MMDAgent_Example-1.8

RUN mkdir -p /var/open_jtalk/voice && \
    cp Voice/mei/mei_normal.htsvoice /var/open_jtalk/voice && \
    rm -rf /var/open_jtalk/voice/MMDAgent_Example-1.8.zip /var/open_jtalk/voice/MMDAgent_Example-1.8

# Install default dictionary

WORKDIR /tmp

RUN mkdir -p /var/open_jtalk/dic

WORKDIR /var/open_jtalk/dic

RUN wget --progress=dot:giga https://downloads.sourceforge.net/open-jtalk/open_jtalk_dic_utf_8-1.11.tar.gz && \
    tar xf open_jtalk_dic_utf_8-1.11.tar.gz && \
    cp open_jtalk_dic_utf_8-1.11/* . && \
    rm -rf open_jtalk_dic_utf_8-1.11.tar.gz open_jtalk_dic_utf_8-1.11

# Build Trinity

WORKDIR /var/trinity

COPY ./pnpm-lock.yaml ./.npmrc ./

RUN pnpm fetch

COPY . .

RUN pnpm install --offline && \
    pnpm run build

ARG NODE_ENV=production

ENV NODE_ENV=${NODE_ENV} DIC_PATH=/var/open_jtalk/dic

RUN pnpm prune

ENTRYPOINT ["gosu", "node"]

CMD ["node", "-r", "dotenv/config", "app/server.js"]
